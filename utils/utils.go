package utils

import (
	"os"
	"path/filepath"
	"strings"

	"github.com/mitchellh/go-homedir"
)

// AbsPathify returns an absolute representation of path.
func AbsPathify(path string) (string, error) {
	if strings.HasPrefix(path, "$HOME") {
		home, err := homedir.Dir()
		if err != nil {
			return "", err
		}
		path = home + path[5:]
	}
	if strings.HasPrefix(path, "~") {
		epath, err := homedir.Expand(path)
		if err != nil {
			return "", err
		}
		path = epath
	}
	if strings.HasPrefix(path, "$") {
		end := strings.Index(path, string(os.PathSeparator))
		path = os.Getenv(path[1:end]) + path[end:]
	}
	if filepath.IsAbs(path) {
		return filepath.Clean(path), nil
	}

	return filepath.Abs(path)
}

// Exists checks if file/directory exists in the file system.
func Exists(path string) (bool, error) {
	_, err := os.Lstat(path)
	if err == nil {
		return true, nil
	}
	if os.IsNotExist(err) {
		return false, nil
	}
	return false, err
}
