package repo

import (
	"fmt"
	"log"
	"os"
	"os/exec"
	"path/filepath"

	"github.com/lnguyen/go-transmission/transmission"

	"gitlab.fel.cvut.cz/mansumax/btvault/btv-node/db"
	"gitlab.fel.cvut.cz/mansumax/btvault/btv-node/torrent"
	"gitlab.fel.cvut.cz/mansumax/btvault/config"
)

const (
	transmissionDir = "transmission"
	torrentDir      = "torrent"
	dataDir         = "data"
	metaDir         = "meta"
)

type Repository struct {
	torrentDir string
	dataDir    string
	metaDir    string
	db         *db.SQLiteDB
	torrentCli *torrentClient
	cfg        *config.Config
}

func InitRepository(cfg *config.Config, port string) (repo *Repository, err error) {
	mkdir := func(name string) (path string) {
		if err != nil {
			return
		}
		path = filepath.Join(cfg.Repository, name)
		_, err = os.Stat(path)
		if os.IsNotExist(err) {
			err = os.MkdirAll(path, 0774)
		}
		return
	}
	transmissionDir := mkdir(transmissionDir)
	torrentDir := mkdir(torrentDir)
	dataDir := mkdir(dataDir)
	metaDir := mkdir(metaDir)
	if err != nil {
		return nil, err
	}

	db, err := db.InitDB(cfg.Repository)
	if err != nil {
		return nil, err
	}

	cli, err := newTorrentClient(port, dataDir, transmissionDir)
	if err != nil {
		return nil, err
	}
	if err = cli.start(); err != nil {
		return nil, err
	}

	return &Repository{
		dataDir:    dataDir,
		metaDir:    metaDir,
		torrentDir: torrentDir,
		torrentCli: cli,
		db:         db,
		cfg:        cfg,
	}, nil
}

func (repo *Repository) AddTorrentFile(name string) error {
	added, err := repo.torrentCli.trans.AddTorrentByFilename(name, repo.dataDir)
	if err != nil {
		return err
	}
	if added.ID != 0 {
		log.Printf("Torrent started: %d %s %s", added.ID, added.Name, added.HashString)
	}
	return nil
}

func (repo *Repository) Terminate() {
	log.Println("terminating repository.")

	repo.torrentCli.stop()
	repo.db.Close()
}

func (repo *Repository) Database() *db.SQLiteDB {
	return repo.db
}

func (repo *Repository) TorrentDir() string {
	return repo.torrentDir
}

func (repo *Repository) DataDir() string {
	return repo.dataDir
}

func (repo *Repository) MetaDir() string {
	return repo.metaDir
}

type torrentClient struct {
	trans transmission.TransmissionClient
	cmd   *exec.Cmd
}

func newTorrentClient(port, dwnDir, cfgDir string) (*torrentClient, error) {
	if err := torrent.DumpTransmissionConfig(cfgDir); err != nil {
		return nil, err
	}
	return &torrentClient{
		trans: transmission.New("http://127.0.0.1:"+port, "", ""),
		cmd: exec.Command(
			"transmission-daemon",
			"--port", port,
			"--download-dir", dwnDir,
			"--config-dir", cfgDir,
			"--no-blocklist",
			"--foreground",
			"--encryption-preferred",
			"--no-global-seedratio",
			"--no-incomplete-dir",
			"--portmap",
			"--dht",
			"--no-auth",
			"--utp",
		),
	}, nil
}

func (cli *torrentClient) start() error {
	log.Printf("starting transmission-daemon on port %s", cli.cmd.Args[2])
	if err := cli.cmd.Start(); err != nil {
		return fmt.Errorf("starting transmission-daemon: %v", err)
	}
	return nil
}

func (cli *torrentClient) stop() {
	log.Printf("stopping transmission-daemon on port %s", cli.cmd.Args[2])
	if err := cli.cmd.Process.Kill(); err != nil {
		log.Fatalln("failed to kill transmission-daemon: ", err)
	}
}
