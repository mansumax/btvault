package repo

import (
	"crypto/rand"
	"encoding/base64"
	"fmt"
	"os"
	"path/filepath"
	"time"

	"gitlab.fel.cvut.cz/mansumax/btvault/btv-node/db"
)

type BackupSetList struct {
	sets []BackupSet
	repo *Repository
}

func (repo *Repository) NewBackupSetList() *BackupSetList {
	// Prepare backup sets.
	sets := make([]BackupSet, 0, 6)
	nowFmt := formatTime(time.Now())
	for i := 0; i < 6; i++ {
		set := BackupSet{
			id:      fmt.Sprintf("%s_%s_%s", repo.cfg.UserID, nowFmt, genRandomStringN(8)),
			fragIDs: make([]string, 0, 6),
		}
		sets = append(sets, set)
	}

	return &BackupSetList{
		sets: sets,
		repo: repo,
	}
}

func (bsl *BackupSetList) Sets() []BackupSet {
	return bsl.sets
}

func (bsl *BackupSetList) Add(n int, id string, data []byte) (string, error) {
	if n < 0 || n >= len(bsl.sets) {
		return "", fmt.Errorf("wrong set number: %d", n)
	}

	path := filepath.Join(bsl.repo.dataDir, bsl.sets[n].id, id)
	// Make directory, if it does not exist yet.
	if err := os.MkdirAll(filepath.Dir(path), 0774); err != nil {
		return "", err
	}
	if err := writeOnDisk(path, data); err != nil {
		return "", err
	}
	if err := bsl.repo.db.InsertFile(db.File{ID: id, Path: path}); err != nil {
		return "", err
	}

	bsl.sets[n].fragIDs = append(bsl.sets[n].fragIDs, id)

	return path, nil
}

type BackupSet struct {
	id      string
	fragIDs []string
}

func (bs *BackupSet) FragIDs() []string {
	return bs.fragIDs
}

func (bs *BackupSet) ID() string {
	return bs.id
}

func genRandomStringN(n int) string {
	b := make([]byte, n)
	rand.Read(b)
	return base64.RawURLEncoding.EncodeToString(b)[:n]
}

func formatTime(t time.Time) string {
	return fmt.Sprintf("%04.4d%02.2d%02.2d%02.2d%02.2d%02.2d",
		t.Year(), t.Month(), t.Day(), t.Hour(), t.Minute(), t.Second())
}
