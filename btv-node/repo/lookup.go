package repo

import (
	"bufio"
	"bytes"
	"errors"
	"fmt"
	"io"
	"io/ioutil"
	"log"
	"net"
	"os"
	"path/filepath"
	"strconv"
	"strings"
)

var ErrNotFound = errors.New("data not found")

func (repo *Repository) LookupMeta(id string) ([]byte, error) {
	log.Println("looking in DHT: ", id)

	// Try to lookup meta-data on the disk first.
	path := filepath.Join(repo.metaDir, id)
	data, err := ioutil.ReadFile(path)
	if err != nil && !os.IsNotExist(err) {
		return nil, fmt.Errorf("reading meta-data: %v", err)
	}
	if err == nil {
		return data, nil
	}

	// Dial DHT.
	conn, err := net.Dial("tcp", repo.cfg.DHTEndpoint)
	if err != nil {
		return nil, fmt.Errorf("dialing DHT: %v", err)
	}
	defer conn.Close()

	// Request meta-data.
	fmt.Fprintf(conn, "GET %s\n", id)

	// Get meta-data size.
	resp, err := bufio.NewReader(conn).ReadString('\n')
	if !strings.HasPrefix(resp, "OK") {
		return nil, ErrNotFound
	}

	size, err := strconv.ParseUint(resp[3:len(resp)-1], 10, 64)
	if err != nil {
		return nil, fmt.Errorf("failed to convert meta-data size %q: %v", resp, err)
	}

	// Get meta-data.
	var buf bytes.Buffer
	_, err = io.CopyN(&buf, conn, int64(size))
	if err != nil {
		return nil, fmt.Errorf("recieving torrent: %v", err)
	}

	return buf.Bytes(), nil
}
