package repo

import (
	"bufio"
	"bytes"
	"encoding/base64"
	"fmt"
	"io"
	"log"
	"net"
	"os"
	"path/filepath"
	"strings"

	"github.com/anacrolix/torrent/metainfo"

	"gitlab.fel.cvut.cz/mansumax/btvault/btv-node/db"
	"gitlab.fel.cvut.cz/mansumax/btvault/btv-node/torrent"
)

func (repo *Repository) StoreMeta(id string, data []byte) (string, error) {
	log.Println("storing in DHT: ", id)

	// Store meta-data on the disk.
	path := filepath.Join(repo.metaDir, id)
	err := writeOnDisk(path, data)
	if err != nil {
		return "", err
	}

	// Dial DHT.
	conn, err := net.Dial("tcp", repo.cfg.DHTEndpoint)
	if err != nil {
		return "", fmt.Errorf("dialing DHT: %v", err)
	}
	defer conn.Close()

	// Send request.
	fmt.Fprintf(conn, "PUT %s %d\n", id, len(data))

	// Receive confirmation.
	resp, err := bufio.NewReader(conn).ReadString('\n')
	if err != nil {
		return "", fmt.Errorf("recieving response from DHT: %v", err)
	}
	if !strings.HasPrefix(resp, "OK") {
		return "", fmt.Errorf("response from DHT: %s", resp)
	}

	// Store meta-data.
	_, err = io.Copy(conn, bytes.NewReader(data))
	if err != nil {
		return "", fmt.Errorf("sending meta-data: %v", err)
	}

	return path, nil
}

func (repo *Repository) StoreTorrent(mi *metainfo.MetaInfo) (string, error) {
	h := mi.HashInfoBytes()
	id := base64.RawURLEncoding.EncodeToString(h[:])

	path := filepath.Join(repo.torrentDir, id+".torrent")
	f, err := os.Create(path)
	if err != nil {
		return "", err
	}
	defer f.Close()

	w := bufio.NewWriter(f)
	defer w.Flush()

	if err = mi.Write(w); err != nil {
		return "", err
	}

	return path, nil
}

func (repo *Repository) StoreBackupSetList(bsl *BackupSetList) ([]db.BackupSet, error) {
	sets := make([]db.BackupSet, 0, len(bsl.Sets()))

	for _, set := range bsl.Sets() {
		dir := filepath.Join(repo.dataDir, set.ID())

		// Create a torrent file from the backup set.
		mi, err := torrent.NewMetaInfoFromDir(dir)
		if err != nil {
			return nil, err
		}
		path, err := repo.StoreTorrent(mi)
		if err != nil {
			return nil, err
		}

		// Add torrent file to the client.
		if err = repo.AddTorrentFile(path); err != nil {
			return nil, fmt.Errorf("adding torrent %q: %v", path, err)
		}

		// Add set into DB.
		info, err := mi.UnmarshalInfo()
		if err != nil {
			return nil, err
		}
		s := db.BackupSet{
			ID:          set.ID(),
			TorrentPath: path,
			Size:        uint64(info.TotalLength()),
		}
		err = repo.db.InsertBackupSet(s)
		if err != nil {
			return nil, err
		}
		sets = append(sets, s)
	}

	return sets, nil
}

func writeOnDisk(name string, data []byte) error {
	f, err := os.Create(name)
	if err != nil {
		return err
	}
	defer f.Close()

	w := bufio.NewWriter(f)
	defer w.Flush()

	_, err = w.Write(data)
	if err != nil {
		return err
	}

	return nil
}
