package db

import (
	"database/sql"
)

type Stored struct {
	ID          int64
	BackupSetID string
	ContractID  string
}

func createStoredTable(db *sql.DB) error {
	stmt := `
	CREATE TABLE IF NOT EXISTS Stored (
		ID             INTEGER PRIMARY KEY,
		BackupSetID    TEXT NOT NULL,
		ContractID     TEXT NOT NULL,
		FOREIGN KEY(BackupSetID) REFERENCES BackupSets(ID),
		FOREIGN KEY(ContractID) REFERENCES Contracts(ID)
	);
	`
	_, err := db.Exec(stmt)
	return err
}

func (s *SQLiteDB) InsertStored(stored Stored) error {
	stmt := `
	INSERT INTO Stored (
		BackupSetID,
		ContractID
	)
	VALUES (?, ?);
	`
	_, err := s.db.Exec(
		stmt,
		stored.BackupSetID,
		stored.ContractID,
	)
	return err
}

func (s *SQLiteDB) SelectUnderstoredBackupSetsBySize(size uint64) ([]BackupSet, error) {
	stmt := `
	SELECT *
	FROM  BackupSets s
	WHERE s.Size=? AND (
		SELECT COUNT(*)
		FROM Stored
		WHERE BackupSetID=s.ID
	)<1;
	`
	rows, err := s.db.Query(stmt, size)
	if err != nil {
		return nil, err
	}
	defer rows.Close()

	return processBackupSetsRows(rows)
}

func (s *SQLiteDB) SelectUnusedContractByBoughtSpace(bought uint64) (Contract, error) {
	stmt := `
	SELECT *
	FROM  Contracts c
	WHERE c.BoughtSpace>=? AND (
		SELECT COUNT(*)
		FROM Stored
		WHERE ContractID=c.ID
	)=0
	ORDER BY c.BoughtSpace ASC
	LIMIT 1;
	`
	row := s.db.QueryRow(stmt, bought)
	return processContractsRow(row)
}

func (s *SQLiteDB) SelectUnusedContract() ([]Contract, error) {
	stmt := `
	SELECT *
	FROM  Contracts c
	WHERE (
		SELECT COUNT(*)
		FROM Stored
		WHERE ContractID=c.ID
	)=0
	ORDER BY c.BoughtSpace ASC;
	`
	rows, err := s.db.Query(stmt)
	if err != nil {
		return nil, err
	}
	defer rows.Close()

	return processContractsRows(rows)
}

func (s *SQLiteDB) IsBackupSetUnderstored(id string) (bool, error) {
	stmt := `
	SELECT COUNT(*)
	FROM Stored
	WHERE BackupSetID=?;
	`
	row := s.db.QueryRow(stmt, id)

	var c int
	if err := row.Scan(&c); err != nil {
		return false, err
	}

	return (c < 1), nil
}
