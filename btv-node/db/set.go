package db

import (
	"database/sql"
	"time"
)

type BackupSet struct {
	ID             string
	TorrentPath    string
	Size           uint64
	insertionDate  time.Time
	lastUpdateDate time.Time
}

func createBackupSetsTable(db *sql.DB) error {
	stmt := `
	CREATE TABLE IF NOT EXISTS BackupSets (
		ID             TEXT NOT NULL PRIMARY KEY,
		TorrentPath    TEXT NOT NULL,
		Size           INTEGER NOT NULL,
		InsertionDate  TIMESTAMP NOT NULL,
		LastUpdateDate TIMESTAMP NOT NULL
	);
	`
	_, err := db.Exec(stmt)
	return err
}

func (s *SQLiteDB) InsertBackupSet(set BackupSet) error {
	stmt := `
	INSERT INTO BackupSets (
		ID,
		TorrentPath,
		Size,
		InsertionDate,
		LastUpdateDate
	)
	VALUES (?, ?, ?, ?, ?);
	`
	now := time.Now()
	_, err := s.db.Exec(
		stmt,
		set.ID,
		set.TorrentPath,
		set.Size,
		now,
		now,
	)
	return err
}

func (s *SQLiteDB) InsertBackupSets(sets []BackupSet) error {
	for _, set := range sets {
		if err := s.InsertBackupSet(set); err != nil {
			return err
		}
	}
	return nil
}

func (s *SQLiteDB) SelectBackupSetByID(id string) (BackupSet, error) {
	stmt := `
	SELECT * FROM BackupSets
	WHERE ID=?;
	`
	row := s.db.QueryRow(
		stmt,
		id,
	)
	return processBackupSetsRow(row)
}

func processBackupSetsRow(row *sql.Row) (BackupSet, error) {
	set := BackupSet{}
	err := row.Scan(
		&set.ID,
		&set.TorrentPath,
		&set.Size,
		&set.insertionDate,
		&set.lastUpdateDate,
	)
	if err != nil {
		return BackupSet{}, err
	}
	return set, nil
}

func processBackupSetsRows(rows *sql.Rows) (sets []BackupSet, err error) {
	for rows.Next() {
		set := BackupSet{}
		err := rows.Scan(
			&set.ID,
			&set.TorrentPath,
			&set.Size,
			&set.insertionDate,
			&set.lastUpdateDate,
		)
		if err != nil {
			return nil, err
		}
		sets = append(sets, set)
	}
	if err = rows.Err(); err != nil {
		return nil, err
	}
	return
}
