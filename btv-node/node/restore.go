package node

import (
	"bufio"
	"bytes"
	"crypto/sha256"
	"database/sql"
	"encoding/base64"
	"fmt"
	"io"
	"io/ioutil"
	"log"
	"os"
	"path/filepath"
	"strconv"
	"strings"
	"time"

	"gitlab.fel.cvut.cz/mansumax/btvault/btv-node/meta"
	"gitlab.fel.cvut.cz/mansumax/btvault/btv-node/repo"
	"gitlab.fel.cvut.cz/mansumax/btvault/btv-node/rscodes"
)

type RestoreRequest struct {
	OutDir string
	IDs    []string
}

type RestoreResponse struct {
	Paths []string
}

func (n *Node) Restore(req RestoreRequest, res *RestoreResponse) error {
	var fl *meta.FileList

	// Lookup the user's FileList (locally and globally).
	// If it exists, then use it. Otherwise, return ErrEmptyFileList.
	flID := meta.GetFileListID(n.cfg)
	data, err := n.repo.LookupMeta(flID)
	if err != nil && err != repo.ErrNotFound {
		return fmt.Errorf("looking up idxID %q: %v", flID, err)
	}
	if data == nil {
		return ErrEmptyFileList
	}

	fl, err = meta.FileListFrom(n.cfg, bytes.NewBuffer(data))
	if err != nil {
		return fmt.Errorf("creating index: %v", err)
	}

	// Restore each file.
	for _, id := range req.IDs {
		if err := n.restoreFile(fl, id, req.OutDir, res); err != nil {
			return fmt.Errorf("restoring %q: %v", id, err)
		}
	}

	return nil
}

func (n *Node) restoreFile(fl *meta.FileList, id string, outDir string, res *RestoreResponse) error {
	var fblID = id
	var ts time.Time

	// Check whether ID contains a timestamp.
	if i := strings.Index(id, idSeparator); i != -1 {
		fblID = id[:i]
		tmp, err := time.Parse(time.RFC3339, id[i+1:])
		if err != nil {
			return ErrWrongTimestamp
		}
		ts = tmp
	}

	// Add the given fblID in the user's index.
	path := fl.HasID(fblID)
	if path == "" {
		return ErrWrongID
	}

	// Lookup a FBL corresponding to the given fblID (locally and globally).
	data, err := n.repo.LookupMeta(fblID)
	if data == nil || err != nil {
		return fmt.Errorf("looking up fblID %q: %v", fblID, err)
	}
	fbl, err := meta.FileBlockListFrom(n.cfg, bytes.NewBuffer(data))
	if err != nil {
		return fmt.Errorf("creating FBL: %v", err)
	}

	// Verify that at least on version exists.
	vers := fbl.Versions()
	if len(vers) == 0 {
		return fmt.Errorf("FBL has no versions")
	}

	// Choose the version.
	// If dateStr is not specified, just pick the latest one.
	var ver meta.Version
	if ts.IsZero() {
		ver = vers[len(vers)-1]
	} else {
		for _, ver = range vers {
			if ts.Equal(ver.Timestamp()) {
				break
			}
		}
	}

	// Prepare the output file.
	fi := fbl.FileInfo()
	outPath := filepath.Join(outDir, path)
	err = os.MkdirAll(filepath.Dir(outPath), 0774)
	if err != nil {
		return err
	}
	f, err := os.Create(outPath)
	if err != nil {
		return err
	}
	if err = f.Chmod(os.FileMode(fi.Mode())); err != nil {
		return err
	}
	defer f.Close()

	// Create write buffer.
	w := bufio.NewWriter(f)
	defer w.Flush()

	// Create block fragmenter.
	fgr, err := rscodes.NewFragmenter()
	if err != nil {
		return err
	}

	for _, block := range ver.Blocks() {
		// Calculate bflID for given block.
		h := sha256.New()
		h.Write(block.Hash())
		bflID := base64.RawURLEncoding.EncodeToString(h.Sum(nil))

		//Lookup a BFL corresponding to the given bflID (locally and globally).
		data, err := n.repo.LookupMeta(bflID)
		if data == nil || err != nil {
			return fmt.Errorf("looking up bflID %q: %v", bflID, err)
		}
		bfl, err := meta.BlockFragmentListFrom(n.cfg, bytes.NewBuffer(data))
		if err != nil {
			return fmt.Errorf("creating BFL: %v", err)
		}

		// Download fragments.
		frags, err := n.downloadFragments(bfl)
		if err != nil {
			return fmt.Errorf("downloading fragments: %v", err)
		}

		// Reconstruct missing fragments.
		data, err = fgr.Reconstruct(frags, int(block.Length()))
		if err != nil {
			return fmt.Errorf("reconstructing missing fragments: %v", err)
		}

		w.Write(data)
	}
	res.Paths = append(res.Paths, outPath)

	return nil
}

func (n *Node) downloadFragments(bfl *meta.BlockFragmentList) ([]rscodes.Fragment, error) {
	frags := make([]rscodes.Fragment, 6)

	succ, fail := 0, 0
	for _, frag := range bfl.Fragments() {
		// Calculate fragID for given block.
		h := sha256.New()
		h.Write(frag.Hash())
		fragID := base64.RawURLEncoding.EncodeToString(h.Sum(nil))

		// Lookup encrypted fragment data corresponding to the given fragID.
		eData, err := retrieveData(n, fragID)
		if eData == nil || err != nil {
			log.Printf("failed to look up fragID %q: %v", fragID, err)
			if fail++; fail > 2 {
				return nil, fmt.Errorf("more than 2 fragments are missing")
			} else {
				continue
			}
		}

		// Decrypt fragment data.
		data, err := convCrypt(eData, frag.Hash())
		if err != nil {
			return nil, err
		}

		// Add fragment.
		frags[succ] = rscodes.Fragment{
			Data:   data,
			Offset: frag.Offset(),
		}
		if succ++; succ >= 4 {
			break
		}
	}

	return frags, nil
}

func retrieveData(n *Node, id string) ([]byte, error) {
	// Try to find data on the disk first.
	database := n.repo.Database()
	file, err := database.SelectFileByID(id)
	if err != nil && err != sql.ErrNoRows {
		return nil, fmt.Errorf("selecting File: %v", err)
	}
	if err == nil {
		data, err := ioutil.ReadFile(file.Path)
		if err != nil && !os.IsNotExist(err) {
			return nil, fmt.Errorf("reading file: %v", err)
		}
		if data != nil {
			return data, nil
		}
	}

	// Lookup HolderList meta info (locally and globally).
	var hl *meta.HolderList
	data, err := n.repo.LookupMeta(id)
	if err != nil {
		return nil, fmt.Errorf("looking up HolderList %q: %v", id, err)
	}
	hl, err = meta.HolderListFrom(n.cfg, bytes.NewBuffer(data))
	if err != nil {
		return nil, fmt.Errorf("creating HolderList: %v", err)
	}

	// Sequentially send retrieve requests to the holders.
	for _, holderID := range hl.HolderIDs() {
		conn, err := n.SendRetrieveRequest(holderID, id)
		if err != nil {
			log.Printf("sending retrieve request to holderID %q: %v\n", holderID, err)
			continue
		}
		defer conn.Close()

		// Get data size.
		tmp, err := bufio.NewReader(conn).ReadString('\n')
		size, err := strconv.ParseUint(tmp[:len(tmp)-1], 10, 64)
		if err != nil {
			return nil, fmt.Errorf("failed to convert torrentSize %q: %v", tmp, err)
		}

		// Get Torrent.
		var buf bytes.Buffer
		_, err = io.CopyN(&buf, conn, int64(size))
		if err != nil {
			return nil, fmt.Errorf("recieving torrent: %v", err)
		}

		return buf.Bytes(), nil
	}

	return nil, fmt.Errorf("file not found")
}
