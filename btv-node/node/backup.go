package node

import (
	"bufio"
	"bytes"
	"crypto/aes"
	"crypto/cipher"
	"crypto/sha256"
	"fmt"
	"io"
	"os"

	"gitlab.fel.cvut.cz/mansumax/btvault/btv-node/meta"
	"gitlab.fel.cvut.cz/mansumax/btvault/btv-node/rabin"
	"gitlab.fel.cvut.cz/mansumax/btvault/btv-node/repo"
	"gitlab.fel.cvut.cz/mansumax/btvault/btv-node/rscodes"
)

type BackupRequest struct {
	Paths []string
}

type BackupResponse struct {
	FileNumber     uint
	BlockNumber    uint
	FragmentNumber uint
}

func (n *Node) Backup(req BackupRequest, res *BackupResponse) error {
	var fl *meta.FileList

	// Lookup the user's FileList (locally and globally).
	// If it exists, then use it. Otherwise, create a new one.
	flID := meta.GetFileListID(n.cfg)
	data, err := n.repo.LookupMeta(flID)
	if err != nil && err != repo.ErrNotFound {
		return fmt.Errorf("looking up %q: %v", flID, err)
	}
	if data == nil {
		fl = meta.NewFileList(n.cfg)
	} else {
		fl, err = meta.FileListFrom(n.cfg, bytes.NewBuffer(data))
		if err != nil {
			return fmt.Errorf("creating FileList: %v", err)
		}
	}

	// Create a new BSL.
	bsl := n.repo.NewBackupSetList()

	// Backup each file.
	for _, path := range req.Paths {
		if err := n.backupFile(path, bsl, fl, res); err != nil {
			return fmt.Errorf("backing up %q: %v", path, err)
		}
	}

	// Store BSL in the repository.
	sets, err := n.repo.StoreBackupSetList(bsl)
	if err != nil {
		return fmt.Errorf("storing BSL: %v", err)
	}

	// Store encrypted FileList the given flID in the repository.
	_, err = n.repo.StoreMeta(flID, []byte(fl.String()+"\n"))
	if err != nil {
		return fmt.Errorf("storing index: %v", err)
	}

	// Start trading.
	go n.StartTrading(sets)

	return nil
}

func (n *Node) backupFile(path string, bsl *repo.BackupSetList, fl *meta.FileList, res *BackupResponse) error {
	f, err := os.Open(path)
	if err != nil {
		return err
	}
	defer f.Close()

	fi, err := f.Stat()
	if err != nil {
		return nil
	}

	// Ignore empty or non-regular files.
	if fi.Size() == 0 || !fi.Mode().IsRegular() {
		return nil
	}

	// Create file slicer.
	slr := rabin.NewSlicer(bufio.NewReader(f))

	// Create block fragmenter.
	fgr, err := rscodes.NewFragmenter()
	if err != nil {
		return err
	}

	// Add the given path to the user's index.
	fblID := fl.HasPath(path)
	if fblID == "" {
		fblID = fl.Add(path)
		res.FileNumber++
	}

	var fbl *meta.FileBlockList

	// Lookup a FBL corresponding to the given fblID (locally and globally).
	// If it exists, then use it. Otherwise, create a new one.
	data, err := n.repo.LookupMeta(fblID)
	if err != nil && err != repo.ErrNotFound {
		return fmt.Errorf("looking up %q: %v", fblID, err)
	}
	if data == nil {
		fbl = meta.NewFileBlockList(n.cfg, fi)
	} else {
		fbl, err = meta.FileBlockListFrom(n.cfg, bytes.NewBuffer(data))
		if err != nil {
			return fmt.Errorf("creating FBL: %v", err)
		}
	}

	// Ignore files that were not modified since the last backup.
	if !isModified(fi, fbl) {
		return nil
	}

	// Create a new file version.
	ver := meta.NewVersion(fi)
	for {
		// Get the next block.
		b, err := slr.NextBlock()
		if err == io.EOF {
			break
		}
		if err != nil {
			return fmt.Errorf("slicer.NextBlock(): %v", err)
		}

		// Add the info about the given block to the version.
		bflID := ver.Add(b.Data, b.Length, b.Offset)

		//Lookup a BFL corresponding to the given bflID (locally and globally).
		//If it exists, then continue to the next block. Otherwise, create a new one.
		data, err := n.repo.LookupMeta(bflID)
		if err != nil && err != repo.ErrNotFound {
			return fmt.Errorf("looking up %q: %v", bflID, err)
		}
		if data != nil {
			continue
		}
		bfl := meta.NewBlockFragmentList(n.cfg)
		res.BlockNumber++

		// Split the block into fragments (including parity fragments).
		frags, err := fgr.Split(b.Data)
		if err != nil {
			return err
		}

		for i, frag := range frags {
			// Add the info about the given fragment to the block's BFL.
			fragID := bfl.Add(frag.Data, frag.Offset)
			res.FragmentNumber++

			// Encrypt the fragments content using convergent encryption.
			eData, err := convCrypt(frag.Data, nil)
			if err != nil {
				return err
			}

			// Store encrypted fragment under the given fragID in the repository.
			_, err = bsl.Add(i, fragID, eData)
			if err != nil {
				return fmt.Errorf("storing fragment: %v", err)
			}
		}

		//Store encrypted BFL the given bflID in the repository.
		_, err = n.repo.StoreMeta(bflID, []byte(bfl.String()+"\n"))
		if err != nil {
			return fmt.Errorf("storing bfl: %v", err)
		}
	}

	// Add the version to the file's FBL.
	fbl.Add(*ver)

	//Store encrypted FBL the given fblID in the repository.
	_, err = n.repo.StoreMeta(fblID, []byte(fbl.String()+"\n"))
	if err != nil {
		return fmt.Errorf("storing FBL: %v", err)
	}

	return nil
}

func convCrypt(in, key []byte) ([]byte, error) {
	if key == nil {
		// We are encrypting, so set the key.
		h := sha256.New()
		h.Write(in)
		key = h.Sum(nil)
	}

	bc, err := aes.NewCipher(key)
	if err != nil {
		return nil, err
	}

	// We start with a zero iv because we're certain that the
	// encryption key is "unique" for every input.
	iv := make([]byte, bc.BlockSize())
	out := make([]byte, len(in))

	ctr := cipher.NewCTR(bc, iv)
	ctr.XORKeyStream(out, in)

	return out, nil
}

func isModified(fi os.FileInfo, fbl *meta.FileBlockList) bool {
	vers := fbl.Versions()
	if len(vers) == 0 {
		return true
	}
	lastVer := vers[len(vers)-1]

	return fi.ModTime().After(lastVer.Timestamp())
}
