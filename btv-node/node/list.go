package node

import (
	"bytes"
	"errors"
	"fmt"
	"time"

	"gitlab.fel.cvut.cz/mansumax/btvault/btv-node/meta"
	"gitlab.fel.cvut.cz/mansumax/btvault/btv-node/repo"

	"code.cloudfoundry.org/bytefmt"
)

const (
	idSeparator = "#"
)

type ListRequest struct {
	ID string
}

type ListResponse struct {
	List [][]string
}

var ErrEmptyFileList = errors.New("user's FileList is empty")
var ErrWrongID = errors.New("invalid id")
var ErrWrongTimestamp = errors.New("invalid version timestamp")

func (n *Node) List(req ListRequest, res *ListResponse) error {
	var fl *meta.FileList

	// Lookup the user's FileList (locally and globally).
	// If it exists, then use it. Otherwise, return ErrEmptyFileList.
	flID := meta.GetFileListID(n.cfg)
	data, err := n.repo.LookupMeta(flID)
	if err != nil && err != repo.ErrNotFound {
		return fmt.Errorf("looking up %q: %v", flID, err)
	}
	if data == nil {
		return ErrEmptyFileList
	}

	fl, err = meta.FileListFrom(n.cfg, bytes.NewBuffer(data))
	if err != nil {
		return fmt.Errorf("creating index: %v", err)
	}

	if req.ID == "" {
		items := fl.Items()
		res.List = make([][]string, 0, len(items))
		for _, item := range items {
			res.List = append(res.List, []string{item.Key, item.Value})
		}
		return nil
	}

	// Lookup a FBL corresponding to the given ID (locally and globally).
	// If it exists, then use it. Otherwise, return ErrWrongID.
	if fl.HasID(req.ID) == "" {
		return ErrWrongID
	}
	data, err = n.repo.LookupMeta(req.ID)
	if err != nil && err != repo.ErrNotFound {
		return err
	}
	if data == nil {
		return fmt.Errorf("failed to lookup the FBL")
	}
	fbl, err := meta.FileBlockListFrom(n.cfg, bytes.NewBuffer(data))
	if err != nil {
		return fmt.Errorf("creating FBL: %v", err)
	}

	vers := fbl.Versions()
	res.List = make([][]string, 0, len(vers))
	for _, ver := range vers {
		res.List = append(res.List, []string{
			req.ID + idSeparator + ver.Timestamp().Format(time.RFC3339),
			bytefmt.ByteSize(ver.Size()),
		})
	}

	return nil
}
