package node

import (
	"log"
	"net"
	"net/http"
	"net/rpc"
)

type NodeRPCServer struct {
	node *NodeRPC
	srv  *http.Server
}

type NodeRPC struct {
	node *Node
}

func NewNodeRPCServer(node *Node, port string) *NodeRPCServer {
	addr := net.JoinHostPort("127.0.0.1", port)
	return &NodeRPCServer{
		node: &NodeRPC{node: node},
		srv:  &http.Server{Addr: addr},
	}
}

func (nrs *NodeRPCServer) Start() {
	log.Printf("starting RPC server on %s\n", nrs.srv.Addr)

	rpc.Register(nrs.node)
	rpc.HandleHTTP()

	nrs.srv.ListenAndServe()
}

func (nrs *NodeRPCServer) Stop() {
	log.Printf("stopping RPC server on %s\n", nrs.srv.Addr)

	if err := nrs.srv.Shutdown(nil); err != nil {
		log.Fatalln("shutdown error: ", err)
	}
}

func (n *NodeRPC) Backup(req BackupRequest, res *BackupResponse) error {
	return n.node.Backup(req, res)
}

func (n *NodeRPC) List(req ListRequest, res *ListResponse) error {
	return n.node.List(req, res)
}

func (n *NodeRPC) Restore(req RestoreRequest, res *RestoreResponse) error {
	return n.node.Restore(req, res)
}
