package node

import (
	"crypto/sha256"
	"encoding/binary"
	"fmt"
	"log"
	"net/url"

	"gitlab.fel.cvut.cz/mansumax/btvault/config"
)

func (n *Node) send(recipID, msg string) error {
	log.Printf("sending message to %q: %s\n", recipID, msg)

	sendURL, err := makeSendURL(n.cfg, recipID, msg)
	if err != nil {
		return fmt.Errorf("making send url: %v", err)
	}
	if _, err := post(sendURL); err != nil {
		return fmt.Errorf("posting send url: %v", err)
	}
	return nil
}

func makeSendURL(cfg *config.Config, recipID, msg string) (string, error) {
	hash, vals := sendRequestHash(
		cfg.UserID,
		cfg.Factotum.PublicKey,
		recipID,
		msg,
	)
	sig, err := cfg.Factotum.Sign(hash)
	if err != nil {
		return "", err
	}
	vals.Add("sigR", sig.R.String())
	vals.Add("sigS", sig.S.String())

	return (&url.URL{
		Scheme:   "http",
		Host:     cfg.ServerEndpoint,
		Path:     "/send",
		RawQuery: vals.Encode(),
	}).String(), nil
}

// sendRequestHash generates a hash of the supplied arguments
// that, when signed, is used to prove that a send request originated
// from the user that owns the supplied private key.
func sendRequestHash(senderID, publicKey, recipID, msg string) ([]byte, url.Values) {
	const magic = "send-request"
	u := url.Values{}
	h := sha256.New()
	h.Write([]byte(magic))
	w := func(key, val string) {
		var l [4]byte
		binary.BigEndian.PutUint32(l[:], uint32(len(val)))
		h.Write(l[:])
		h.Write([]byte(val))
		u.Add(key, val)
	}
	w("sender", senderID)
	w("key", publicKey)
	w("recip", recipID)
	w("msg", msg)

	return h.Sum(nil), u
}
