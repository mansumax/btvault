package node

import (
	"crypto/sha256"
	"encoding/binary"
	"fmt"
	"log"
	"net/url"

	"gitlab.fel.cvut.cz/mansumax/btvault/config"
)

func (n *Node) broadcast(msg string) error {
	log.Printf("broadcasting message: %s\n", msg)

	broadcastURL, err := makeBroadcastURL(n.cfg, msg)
	if err != nil {
		return fmt.Errorf("making broadcast url: %v", err)
	}
	if _, err := post(broadcastURL); err != nil {
		return fmt.Errorf("posting broadcast url: %v", err)
	}
	return nil
}

func makeBroadcastURL(cfg *config.Config, msg string) (string, error) {
	hash, vals := broadcastRequestHash(
		cfg.UserID,
		cfg.Factotum.PublicKey,
		msg,
	)
	sig, err := cfg.Factotum.Sign(hash)
	if err != nil {
		return "", err
	}
	vals.Add("sigR", sig.R.String())
	vals.Add("sigS", sig.S.String())

	return (&url.URL{
		Scheme:   "http",
		Host:     cfg.ServerEndpoint,
		Path:     "/broadcast",
		RawQuery: vals.Encode(),
	}).String(), nil
}

// broadcastRequestHash generates a hash of the supplied arguments
// that, when signed, is used to prove that a broadcast request originated
// from the user that owns the supplied private key.
func broadcastRequestHash(senderID, publicKey, msg string) ([]byte, url.Values) {
	const magic = "broadcast-request"
	u := url.Values{}
	h := sha256.New()
	h.Write([]byte(magic))
	w := func(key, val string) {
		var l [4]byte
		binary.BigEndian.PutUint32(l[:], uint32(len(val)))
		h.Write(l[:])
		h.Write([]byte(val))
		u.Add(key, val)
	}
	w("sender", senderID)
	w("key", publicKey)
	w("msg", msg)

	return h.Sum(nil), u
}
