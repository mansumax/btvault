package node

import (
	"crypto/sha256"
	"encoding/binary"
	"fmt"
	"io/ioutil"
	"net/http"
	"net/url"
	"strconv"
	"strings"

	"gitlab.fel.cvut.cz/mansumax/btvault/config"
)

func (n *Node) reputation(nodeID string) (float64, error) {
	reputationURL, err := makeReputationURL(n.cfg, nodeID)
	if err != nil {
		return 0.0, fmt.Errorf("making reputation url: %v", err)
	}

	resp, err := get(reputationURL)
	if err != nil {
		return 0.0, fmt.Errorf("posting reputation url: %v", err)
	}

	rep, err := strconv.ParseFloat(strings.TrimSpace(resp), 64)
	if err != nil {
		return 0.0, fmt.Errorf("converting reputation: %v", err)
	}

	return rep, nil
}

func makeReputationURL(cfg *config.Config, nodeID string) (string, error) {
	hash, vals := reputationRequestHash(
		cfg.UserID,
		cfg.Factotum.PublicKey,
		nodeID,
	)
	sig, err := cfg.Factotum.Sign(hash)
	if err != nil {
		return "", err
	}
	vals.Add("sigR", sig.R.String())
	vals.Add("sigS", sig.S.String())

	return (&url.URL{
		Scheme:   "http",
		Host:     cfg.ServerEndpoint,
		Path:     "/reputation",
		RawQuery: vals.Encode(),
	}).String(), nil
}

func get(url string) (string, error) {
	r, err := http.Get(url)
	if err != nil {
		return "", err
	}
	b, err := ioutil.ReadAll(r.Body)
	r.Body.Close()
	if err != nil {
		return "", err
	}
	if r.StatusCode != http.StatusOK {
		return "", fmt.Errorf("server error: %s", string(b))
	}
	return string(b), nil
}

// reputationRequestHash generates a hash of the supplied arguments
// that, when signed, is used to prove that a send request originated
// from the user that owns the supplied private key.
func reputationRequestHash(senderID, publicKey, userID string) ([]byte, url.Values) {
	const magic = "reputation-request"
	u := url.Values{}
	h := sha256.New()
	h.Write([]byte(magic))
	w := func(key, val string) {
		var l [4]byte
		binary.BigEndian.PutUint32(l[:], uint32(len(val)))
		h.Write(l[:])
		h.Write([]byte(val))
		u.Add(key, val)
	}
	w("sender", senderID)
	w("key", publicKey)
	w("user", userID)

	return h.Sum(nil), u
}
