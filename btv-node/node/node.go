package node

import (
	"fmt"
	"io/ioutil"
	"log"
	"net"
	"path/filepath"
	"strconv"

	"gitlab.fel.cvut.cz/mansumax/btvault/btv-node/repo"
	"gitlab.fel.cvut.cz/mansumax/btvault/config"
)

type Node struct {
	repo *repo.Repository
	cfg  *config.Config
	port string
	quit chan bool
}

func InitNode(cfg *config.Config) (*Node, error) {
	log.Printf("initializing node: %s\n", cfg.UserID)

	port, err := getPort()
	if err != nil {
		return nil, fmt.Errorf("choosing port: %v", err)
	}
	repo, err := repo.InitRepository(cfg, strconv.Itoa(port))
	if err != nil {
		return nil, fmt.Errorf("initializing repository: %v", err)
	}

	// Load torrents.
	files, err := ioutil.ReadDir(repo.TorrentDir())
	if err != nil {
		return nil, fmt.Errorf("listing directory: %v", err)
	}
	for _, f := range files {
		path := filepath.Join(repo.TorrentDir(), f.Name())
		if err = repo.AddTorrentFile(path); err != nil {
			return nil, fmt.Errorf("adding torrent %q: %v", path, err)
		}
	}

	port, err = getPort()
	if err != nil {
		return nil, fmt.Errorf("choosing port: %v", err)
	}
	return &Node{
		repo: repo,
		cfg:  cfg,
		port: strconv.Itoa(port),
		quit: make(chan bool, 1),
	}, nil
}

func (n *Node) Terminate() {
	log.Printf("terminating node: %s\n", n.cfg.UserID)
	n.quit <- true

	n.repo.Terminate()
}

func getPort() (int, error) {
	addr, err := net.ResolveTCPAddr("tcp", "127.0.0.1:0")
	if err != nil {
		return 0, err
	}

	l, err := net.ListenTCP("tcp", addr)
	if err != nil {
		return 0, err
	}
	defer l.Close()

	return l.Addr().(*net.TCPAddr).Port, nil
}
