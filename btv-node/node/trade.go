package node

import (
	"bytes"
	"crypto/rand"
	"crypto/sha256"
	"database/sql"
	"encoding/base64"
	"fmt"
	"log"
	"math"
	"net"
	"strconv"
	"strings"
	"time"

	"gitlab.fel.cvut.cz/mansumax/btvault/btv-node/db"
	"gitlab.fel.cvut.cz/mansumax/btvault/btv-node/meta"
	"gitlab.fel.cvut.cz/mansumax/btvault/btv-node/repo"
)

var tradeSubcommands = map[string]func(*Node, net.Conn, []string) error{
	"REQUEST": (*Node).tradeRequest,
	"OFFER":   (*Node).tradeOffer,
	"ACCEPT":  (*Node).tradeAccept,
}

func (n *Node) StartTrading(sets []db.BackupSet) {
	database := n.repo.Database()
	for {
		log.Println("starting trading process")

		// Get list of understored sets.
		understoredSets := make([]db.BackupSet, 0, len(sets))
		for _, set := range sets {
			ok, err := database.IsBackupSetUnderstored(set.ID)
			if err != nil {
				log.Printf("checking whether BackupSet is understored: %v\n", err)
				break
			}
			if ok {
				understoredSets = append(understoredSets, set)
			}
		}
		if len(understoredSets) == 0 {
			// We are done.
			log.Println("ending trading process")
			return
		}

		// Get list of unused contract.
		unusedConts, err := database.SelectUnusedContract()
		if err != nil && err != sql.ErrNoRows {
			log.Printf("selecting unused contracts: %v\n", err)
			break
		}

		visited := make(map[string]bool)

	setLoop:
		for _, set := range understoredSets {
			for _, cont := range unusedConts {
				if !visited[cont.ID] && cont.BoughtSpace >= set.Size {
					// Send store request.
					n.SendStoreRequest(&cont, &set)
					visited[cont.ID] = true
					continue setLoop
				}
			}
			// Broadcast trade request.
			n.BroadcastTradeRequest(&set)
		}

		// Wait and try again.
		select {
		case <-time.Tick(1 * time.Minute):
			continue
		case <-n.quit:
			break
		}
	}
}

func (n *Node) BroadcastTradeRequest(set *db.BackupSet) {
	msg := fmt.Sprintf("TRADE REQUEST %s %d\n", n.cfg.UserID, set.Size)
	if err := n.broadcast(msg); err != nil {
		log.Printf("failed to broadcast the message %q: %v\n", msg, err)
	}
}

func (n *Node) trade(conn net.Conn, args []string) error {
	if len(args) == 0 {
		return fmt.Errorf("wrong number of arguments")
	}
	sub := strings.ToUpper(args[0])
	if f, ok := tradeSubcommands[sub]; ok {
		if err := f(n, conn, args[1:]); err != nil {
			return fmt.Errorf("%s: %v", sub, err)
		}
		return nil
	}

	return fmt.Errorf("invalid arguments: %v", args)
}

func (n *Node) tradeRequest(conn net.Conn, args []string) (err error) {
	if len(args) != 2 {
		return fmt.Errorf("wrong number of arguments")
	}

	// Create a new contract.
	cont := db.Contract{}

	cont.PartnerID = args[0]

	if cont.SoldSpace, err = strconv.ParseUint(args[1], 10, 64); err != nil {
		return fmt.Errorf("failed to convert SoldSpace size %q: %v", args[1], err)
	}

	if cont.BoughtSpace, err = n.calcOptimalSpace(n.cfg.UserID, cont.PartnerID, cont.SoldSpace); err != nil {
		return fmt.Errorf("calculating optimal BoughtSpace size: %v", err)
	}

	cont.FromDate = time.Now()

	cont.ToDate = cont.FromDate.AddDate(0, 0, 14)

	// Generate ContractID.
	entropy := make([]byte, 32)
	if _, err = rand.Read(entropy); err != nil {
		return fmt.Errorf("reading random bytes: %v", err)
	}
	h := sha256.New()
	h.Write(entropy)
	cont.ID = base64.RawURLEncoding.EncodeToString(h.Sum(nil))

	//TODO check available space and preferred ratio.
	// For now, everyone accepts everything.

	// Send a trade offer.
	msg := fmt.Sprintf(
		"TRADE OFFER %s %s %d %d %s %s\n",
		n.cfg.UserID,
		cont.ID,
		cont.SoldSpace,
		cont.BoughtSpace,
		cont.FromDate.Format(time.RFC3339),
		cont.ToDate.Format(time.RFC3339),
	)
	if err = n.send(cont.PartnerID, msg); err != nil {
		return fmt.Errorf("sending trade offer: %v", err)
	}

	// Insert contract into DB.
	database := n.repo.Database()
	if err = database.InsertContract(cont); err != nil {
		return fmt.Errorf("inserting contract into db: %v", err)
	}

	return nil
}

func (n *Node) tradeOffer(conn net.Conn, args []string) (err error) {
	if len(args) != 6 {
		return fmt.Errorf("wrong number of arguments")
	}

	// Create a new contract.
	cont := db.Contract{}

	cont.PartnerID = args[0]

	cont.ID = args[1]

	if cont.BoughtSpace, err = strconv.ParseUint(args[2], 10, 64); err != nil {
		return fmt.Errorf("failed to convert BoughtSpace size %q: %v", args[2], err)
	}

	if cont.SoldSpace, err = strconv.ParseUint(args[3], 10, 64); err != nil {
		return fmt.Errorf("failed to convert SoldSpace size %q: %v", args[3], err)
	}

	if cont.FromDate, err = time.Parse(time.RFC3339, args[4]); err != nil {
		return fmt.Errorf("failed to convert FromDate %q: %v", args[4], err)
	}

	if cont.ToDate, err = time.Parse(time.RFC3339, args[5]); err != nil {
		return fmt.Errorf("failed to convert ToDate %q: %v", args[5], err)
	}

	// Verify whether we are still interested in trading.
	database := n.repo.Database()
	_, err = database.SelectUnderstoredBackupSetsBySize(cont.BoughtSpace)
	if err != nil && err != sql.ErrNoRows {
		return fmt.Errorf("db error: %v", err)
	}
	if err == sql.ErrNoRows {
		// We are not interested in trading anymore, so just ignore.
		return nil
	}

	//TODO check available space and preferred ratio.
	// For now, everyone accepts everything.

	// Send a trade accept.
	msg := fmt.Sprintf("TRADE ACCEPT %s %s\n", n.cfg.UserID, cont.ID)
	if err = n.send(cont.PartnerID, msg); err != nil {
		return fmt.Errorf("sending trade accept: %v", err)
	}

	// Insert contract into DB.
	cont.Accepted = true
	if err = database.InsertContract(cont); err != nil {
		return fmt.Errorf("inserting contract into db: %v", err)
	}

	// Insert partner into DB.
	if err = database.InsertPartnerIfNotPresent(db.Partner{ID: cont.PartnerID}); err != nil {
		return fmt.Errorf("inserting partner into db: %v", err)
	}

	// Store contract in the repository.
	metaCont := meta.NewContract(n.cfg, cont.PartnerID, cont.SoldSpace, cont.BoughtSpace)
	metaContID := meta.GetContractID(n.cfg, cont.ID)
	if _, err = n.repo.StoreMeta(metaContID, []byte(metaCont.String()+"\n")); err != nil {
		return fmt.Errorf("storing Contract: %v", err)
	}

	// Lookup the user's ContractList (locally and globally).
	// If it exists, then use it. Otherwise, create a new one.
	var cl *meta.ContractList
	clID := meta.GetContractListID(n.cfg)
	data, err := n.repo.LookupMeta(clID)
	if err != nil && err != repo.ErrNotFound {
		return fmt.Errorf("looking up clID %q: %v", clID, err)
	}
	if data == nil {
		cl = meta.NewContractList(n.cfg)
	} else {
		cl, err = meta.ContractListFrom(n.cfg, bytes.NewBuffer(data))
		if err != nil {
			return fmt.Errorf("creating ContractList: %v", err)
		}
	}

	// Add contract to the ContractList.
	cl.Add(cont.ID)

	// Store ContractList in the repository.
	if _, err = n.repo.StoreMeta(clID, []byte(cl.String()+"\n")); err != nil {
		return fmt.Errorf("storing ContractList: %v", err)
	}

	return nil
}

func (n *Node) tradeAccept(conn net.Conn, args []string) (err error) {
	if len(args) != 2 {
		return fmt.Errorf("wrong number of arguments")
	}

	// Find contract in DB.
	database := n.repo.Database()
	cont, err := database.SelectContractByID(args[1])
	if err != nil {
		return fmt.Errorf("selecting contract by id=%q: %v", args[1], err)
	}

	// Mark contract as accepted.
	cont.Accepted = true
	if err = database.UpdateContract(cont); err != nil {
		return fmt.Errorf("updating contract with id=%q: %v", cont.ID, err)
	}

	// Insert partner into DB.
	if err = database.InsertPartnerIfNotPresent(db.Partner{ID: cont.PartnerID}); err != nil {
		return fmt.Errorf("inserting partner into db: %v", err)
	}

	// Store contract in the repository.
	metaCont := meta.NewContract(n.cfg, cont.PartnerID, cont.SoldSpace, cont.BoughtSpace)
	metaContID := meta.GetContractID(n.cfg, cont.ID)
	if _, err = n.repo.StoreMeta(metaContID, []byte(metaCont.String()+"\n")); err != nil {
		return fmt.Errorf("storing Contract: %v", err)
	}

	// Lookup the user's ContractList (locally and globally).
	// If it exists, then use it. Otherwise, create a new one.
	var cl *meta.ContractList
	clID := meta.GetContractListID(n.cfg)
	data, err := n.repo.LookupMeta(clID)
	if err != nil && err != repo.ErrNotFound {
		return fmt.Errorf("looking up clID %q: %v", clID, err)
	}
	if data == nil {
		cl = meta.NewContractList(n.cfg)
	} else {
		cl, err = meta.ContractListFrom(n.cfg, bytes.NewBuffer(data))
		if err != nil {
			return fmt.Errorf("creating ContractList: %v", err)
		}
	}

	// Add contract to the ContractList.
	cl.Add(cont.ID)

	// Store ContractList in the repository.
	if _, err = n.repo.StoreMeta(clID, []byte(cl.String()+"\n")); err != nil {
		return fmt.Errorf("storing ContractList: %v", err)
	}

	return nil
}

func (n *Node) calcOptimalSpace(idA, idB string, sA uint64) (uint64, error) {
	// Obtain ratings.
	rA, err := n.reputation(idA)
	if err != nil {
		return 0, fmt.Errorf("requesting mine reputation: %v", err)
	}
	rB, err := n.reputation(idB)
	if err != nil {
		return 0, fmt.Errorf("requesting his reputation: %v", err)
	}

	// Calculate optimal ratio.
	k := math.Log(1.0-rA) / math.Log(1.0-rB)

	// Calculate space.
	sB := uint64(float64(sA) * k)

	return sB, nil
}
