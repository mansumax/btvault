package meta

import (
	"bufio"
	"crypto"
	"crypto/sha256"
	"encoding/base64"
	"encoding/csv"
	"errors"
	"fmt"
	"io"
	"os"
	"sort"
	"strconv"
	"strings"
	"time"

	"gitlab.fel.cvut.cz/mansumax/btvault/config"
)

//DHTBlob from FileBlockList (FBL):
//	ID        = base64(sha256(privateKey & path))
//	Content   = aes(FBL, kdf(privateKey))
//	Signature = sign(sha256(Content)) & publicKey
//	Data      = Signature & Content

func GetFileBlockListID(cfg *config.Config, path string) string {
	h := cfg.Factotum.PrivateKeyHash(crypto.SHA256)
	h.Write([]byte(path))

	id := base64.RawURLEncoding.EncodeToString(h.Sum(nil))

	return id
}

type FileBlockList struct {
	fi   FileInfo
	vers VersionList
	cfg  *config.Config
}

func NewFileBlockList(cfg *config.Config, fi os.FileInfo) *FileBlockList {
	return &FileBlockList{
		fi:   *NewFileInfo(fi.Name(), fi.ModTime(), uint32(fi.Mode())),
		vers: make(VersionList, 0, 12),
		cfg:  cfg,
	}
}

func FileBlockListFrom(cfg *config.Config, r io.Reader) (*FileBlockList, error) {
	s := bufio.NewScanner(r)

	if !s.Scan() {
		return nil, fmt.Errorf("invalid FileBlockList format: %v", s.Err())
	}
	fi, err := FileInfoFrom(strings.NewReader(s.Text()))
	if err != nil {
		return nil, fmt.Errorf("parsing FileInfo %q: %v", s.Text(), err)
	}

	vers := make(VersionList, 0, 12)
	for s.Scan() {
		ver, err := VersionFrom(strings.NewReader(s.Text()))
		if err != nil {
			return nil, fmt.Errorf("parsing Version %q: %v", s.Text(), err)
		}
		vers = append(vers, *ver)
	}
	if err := s.Err(); err != nil {
		return nil, err
	}

	return &FileBlockList{
		fi:   *fi,
		vers: vers,
		cfg:  cfg,
	}, nil
}

func (fbl *FileBlockList) FileInfo() FileInfo {
	return fbl.fi
}

func (fbl *FileBlockList) Versions() VersionList {
	sort.Sort(fbl.vers)
	return fbl.vers
}

func (fbl *FileBlockList) Add(ver Version) {
	fbl.vers = append(fbl.vers, ver)
}

func (fbl *FileBlockList) String() string {
	recs := make([]string, 0, len(fbl.vers)+1)

	recs = append(recs, fbl.fi.String())

	for _, ver := range fbl.vers {
		recs = append(recs, ver.String())
	}

	return strings.Join(recs, "\n")
}

type FileInfo struct {
	name    string
	modTime time.Time
	mode    uint32
}

func NewFileInfo(name string, modTime time.Time, mode uint32) *FileInfo {
	return &FileInfo{
		name:    name,
		modTime: modTime,
		mode:    mode,
	}
}

func FileInfoFrom(r io.Reader) (*FileInfo, error) {
	reader := csv.NewReader(r)
	recs, err := reader.Read()
	if err != nil {
		return nil, err
	}
	if len(recs) != 3 {
		return nil, errors.New("invalid FileInfo format")
	}

	name := recs[0]

	modTime, err := time.Parse(time.RFC3339, recs[1])
	if err != nil {
		return nil, fmt.Errorf("parsing modification time %q: %v", recs[1], err)
	}

	mode, err := strconv.ParseUint(recs[2], 8, 32)
	if err != nil {
		return nil, fmt.Errorf("converting file mode %q: %v", recs[2], err)
	}

	return NewFileInfo(name, modTime, uint32(mode)), nil
}

func (fi *FileInfo) Name() string {
	return fi.name
}

func (fi *FileInfo) ModTime() time.Time {
	return fi.modTime
}

func (fi *FileInfo) Mode() uint32 {
	return fi.mode
}

func (fi *FileInfo) String() string {
	return fmt.Sprintf("%q,%s,%o", fi.name, fi.modTime.Format(time.RFC3339), fi.mode)
}

type VersionList []Version

func (vl VersionList) Len() int           { return len(vl) }
func (vl VersionList) Swap(i, j int)      { vl[i], vl[j] = vl[j], vl[i] }
func (vl VersionList) Less(i, j int) bool { return vl[i].ts.Before(vl[j].ts) }

type Version struct {
	size   uint64
	ts     time.Time
	blocks BlockList
}

func NewVersion(fi os.FileInfo) *Version {
	return &Version{
		ts:     time.Now(),
		size:   uint64(fi.Size()),
		blocks: make(BlockList, 0, 12),
	}
}

func VersionFrom(r io.Reader) (*Version, error) {
	reader := csv.NewReader(r)
	recs, err := reader.Read()
	if err != nil {
		return nil, err
	}
	if len(recs) < 3 {
		return nil, fmt.Errorf("invalid Version format %q", recs)
	}

	ts, err := time.Parse(time.RFC3339, recs[0])
	if err != nil {
		return nil, fmt.Errorf("parsing timestamp %q: %v", recs[0], err)
	}

	size, err := strconv.ParseUint(recs[1], 10, 64)
	if err != nil {
		return nil, fmt.Errorf("parsing size %q: %v", recs[1], err)
	}

	blocks := make(BlockList, 0, len(recs)-2)
	for _, rec := range recs[2:] {
		bi, err := BlockInfoFrom(strings.NewReader(rec))
		if err != nil {
			return nil, fmt.Errorf("parsing BlockInfo %q: %v", rec, err)
		}
		blocks = append(blocks, *bi)
	}

	return &Version{
		ts:     ts,
		size:   size,
		blocks: blocks,
	}, nil
}

func (s *Version) Blocks() BlockList {
	return s.blocks
}

func (s *Version) Size() uint64 {
	return s.size
}

func (s *Version) Timestamp() time.Time {
	return s.ts
}

func (s *Version) Add(data []byte, length, offset uint) string {
	h := sha256.New()
	h.Write(data)
	hash := h.Sum(nil)

	bi := NewBlockInfo(hash, length, offset)
	s.blocks = append(s.blocks, *bi)

	h = sha256.New()
	h.Write(hash)
	id := base64.RawURLEncoding.EncodeToString(h.Sum(nil))

	return id
}

func (s *Version) String() string {
	recs := make([]string, 0, len(s.blocks)+2)

	recs = append(recs, s.ts.Format(time.RFC3339))
	recs = append(recs, strconv.FormatUint(s.size, 10))

	for _, bi := range s.blocks {
		recs = append(recs, fmt.Sprintf("%q", bi.String()))
	}

	return strings.Join(recs, ",")
}

type BlockList []BlockInfo

type BlockInfo struct {
	hash   []byte
	length uint
	offset uint
}

func NewBlockInfo(hash []byte, length, offset uint) *BlockInfo {
	return &BlockInfo{
		hash:   hash,
		length: length,
		offset: offset,
	}
}

func BlockInfoFrom(r io.Reader) (*BlockInfo, error) {
	rd := csv.NewReader(r)
	recs, err := rd.Read()
	if err != nil {
		return nil, err
	}
	if len(recs) != 3 {
		return nil, fmt.Errorf("invalid BlockInfo format: %q", recs)
	}

	hash, err := base64.RawURLEncoding.DecodeString(recs[0])
	if err != nil {
		return nil, fmt.Errorf("decoding block hash %q: %v", recs[0], err)
	}

	length, err := strconv.ParseUint(recs[1], 10, 32)
	if err != nil {
		return nil, fmt.Errorf("converting block length %q: %v", recs[1], err)
	}

	offset, err := strconv.ParseUint(recs[2], 10, 32)
	if err != nil {
		return nil, fmt.Errorf("converting block offset %q: %v", recs[2], err)
	}

	return NewBlockInfo(hash, uint(length), uint(offset)), nil
}

func (bi *BlockInfo) Hash() []byte {
	return bi.hash
}

func (bi *BlockInfo) Length() uint {
	return bi.length
}

func (bi *BlockInfo) Offset() uint {
	return bi.offset
}

func (bi *BlockInfo) String() string {
	return fmt.Sprintf("%s,%d,%d", base64.RawURLEncoding.EncodeToString(bi.hash), bi.length, bi.offset)
}
