package meta

import (
	"crypto/sha256"
	"encoding/base64"
	"encoding/csv"
	"fmt"
	"io"
	"strconv"
	"strings"

	"gitlab.fel.cvut.cz/mansumax/btvault/config"
)

//DHTBlob from BlockFragmentList (BFL) for Block (B)
//	ID        = base64(sha256(sha256(B)))
//	Content   = aes(BFL, sha256(B))
//	Signature = sign(sha256(Content)) & publicKey
//	Data      = Signature & Content

type BlockFragmentList struct {
	frags FragmentList
	cfg   *config.Config
}

func NewBlockFragmentList(cfg *config.Config) *BlockFragmentList {
	return &BlockFragmentList{
		frags: make(FragmentList, 0, 12),
		cfg:   cfg,
	}
}

func BlockFragmentListFrom(cfg *config.Config, r io.Reader) (*BlockFragmentList, error) {
	bfl := NewBlockFragmentList(cfg)

	rd := csv.NewReader(r)
	recs, err := rd.ReadAll()
	if err != nil {
		return nil, fmt.Errorf("reading CSV: %v", err)
	}
	if len(recs) > 0 && len(recs[0]) != 1 {
		return nil, fmt.Errorf("invalid Index format")
	}
	for _, rec := range recs {
		fi, err := FragmentInfoFrom(strings.NewReader(rec[0]))
		if err != nil {
			return nil, fmt.Errorf("parsing FragmentInfo %q: %v", rec[0], err)
		}
		bfl.frags = append(bfl.frags, *fi)
	}

	return bfl, nil
}

func (bfl *BlockFragmentList) Add(data []byte, offset uint) string {
	h := sha256.New()
	h.Write(data)
	hash := h.Sum(nil)

	fi := NewFragmentInfo(hash, uint(len(data)), offset)
	bfl.frags = append(bfl.frags, *fi)

	h = sha256.New()
	h.Write(hash)
	id := base64.RawURLEncoding.EncodeToString(h.Sum(nil))

	return id
}

func (bfl *BlockFragmentList) Fragments() FragmentList {
	return bfl.frags
}

func (bfl *BlockFragmentList) String() string {
	recs := make([]string, 0, len(bfl.frags))

	for _, frag := range bfl.frags {
		recs = append(recs, fmt.Sprintf("%q", frag.String()))
	}

	return strings.Join(recs, "\n")
}

type FragmentList []FragmentInfo

type FragmentInfo struct {
	hash   []byte
	size   uint
	offset uint
}

func NewFragmentInfo(hash []byte, size, offset uint) *FragmentInfo {
	return &FragmentInfo{
		hash:   hash,
		size:   size,
		offset: offset,
	}
}

func FragmentInfoFrom(r io.Reader) (*FragmentInfo, error) {
	rd := csv.NewReader(r)
	recs, err := rd.Read()
	if err != nil {
		return nil, err
	}
	if len(recs) != 3 {
		return nil, fmt.Errorf("invalid FragmentInfo format: %q", recs)
	}

	hash, err := base64.RawURLEncoding.DecodeString(recs[0])
	if err != nil {
		return nil, fmt.Errorf("decoding fragment hash %q: %v", recs[0], err)
	}

	size, err := strconv.ParseUint(recs[1], 10, 32)
	if err != nil {
		return nil, fmt.Errorf("converting fragment size %q: %v", recs[1], err)
	}

	offset, err := strconv.ParseUint(recs[2], 10, 32)
	if err != nil {
		return nil, fmt.Errorf("converting fragment offset %q: %v", recs[2], err)
	}

	return NewFragmentInfo(hash, uint(size), uint(offset)), nil
}

func (fi *FragmentInfo) Hash() []byte {
	return fi.hash
}

func (fi *FragmentInfo) Size() uint {
	return fi.size
}

func (fi *FragmentInfo) Offset() uint {
	return fi.offset
}

func (fi *FragmentInfo) String() string {
	return fmt.Sprintf("%s,%d,%d",
		base64.RawURLEncoding.EncodeToString(fi.hash), fi.size, fi.offset)
}
