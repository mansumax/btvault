package meta

import (
	"encoding/csv"
	"fmt"
	"io"
	"strings"

	"gitlab.fel.cvut.cz/mansumax/btvault/config"
)

//DHTBlob from HolderList (HL) for Fragment (F):
//	ID         = base64(sha256(sha256(F)))
//	Content    = aes(HL, sha256(F))
//	Signature  = sign(sha256(Content)) & publicKey
//	Data       = Signature & Content

type HolderList struct {
	holderIDs []string
	cfg       *config.Config
}

func NewHolderList(cfg *config.Config) *HolderList {
	return &HolderList{
		holderIDs: make([]string, 0, 12),
		cfg:       cfg,
	}
}

func HolderListFrom(cfg *config.Config, r io.Reader) (*HolderList, error) {
	rd := csv.NewReader(r)
	recs, err := rd.ReadAll()
	if err != nil {
		return nil, fmt.Errorf("reading CSV: %v", err)
	}
	if len(recs) != 1 {
		return nil, fmt.Errorf("invalid HolderList format: %q", recs)
	}

	return &HolderList{
		holderIDs: recs[0],
		cfg:       cfg,
	}, nil
}

func (hl *HolderList) String() string {
	return strings.Join(hl.holderIDs, ",")
}

func (hl *HolderList) Add(userID string) {
	hl.holderIDs = append(hl.holderIDs, userID)
}

func (hl *HolderList) HolderIDs() []string {
	return hl.holderIDs
}
