package rabin

import (
	"bytes"
	"crypto/sha256"
	"encoding/base64"
	"fmt"
	"io"
	"math/rand"
	"testing"
)

func TestRabinSlicing(t *testing.T) {
	size := 32 * miB
	// Generate 32MiB of deterministic pseudo-random data.
	data, err := getRandom(123, size)
	if err != nil {
		t.Error(err)
	}

	// Create slicer.
	slicer := NewSlicer(bytes.NewReader(data))

	// Slice the data and count number of blocks.
	cnt := 0
	for {
		_, err := slicer.NextBlock()
		if err != nil {
			if err == io.EOF {
				break
			}
			t.Error(err)
		}
		cnt++
	}

	avgSize := float32(size) / float32(cnt*miB)
	if (avgSize < 0.5) || (avgSize > 3.0) {
		t.Error(fmt.Sprintf("Average block size is %.2f, but expected it to be within [0.5MiB; 3MiB].", avgSize))
	}
}

func TestRabinSlicingReuse(t *testing.T) {
	blocks := make(map[string]bool)

	// Generate 32MiB of deterministic pseudo-random data.
	data, err := getRandom(123, 32*miB)
	if err != nil {
		t.Error(err)
	}

	// Create slicer.
	slicer := NewSlicer(bytes.NewReader(data))

	// Slice the data.
	for {
		block, err := slicer.NextBlock()
		if err != nil {
			if err == io.EOF {
				break
			}
			t.Error(err)
		}
		hash := sha256.Sum256(block.Data)
		blocks[base64.StdEncoding.EncodeToString(hash[:])] = true
	}

	// Prepend data with 2KiB of newly generated data.
	prefix, err := getRandom(321, 2*kiB)
	if err != nil {
		t.Error(err)
	}
	data = append(prefix, data...)

	// Reset slicer.
	slicer.Reset(bytes.NewReader(data))

	// Slice the modified data and count the number of new blocks.
	cnt := 0
	for {
		block, err := slicer.NextBlock()
		if err != nil {
			if err == io.EOF {
				break
			}
			t.Error(err)
		}
		hash := sha256.Sum256(block.Data)
		if !blocks[base64.StdEncoding.EncodeToString(hash[:])] {
			cnt++
		}
	}
	if cnt > 2 {
		t.Error(fmt.Sprintf("Number of new blocks is %d, but expected it to be within [1;2].", cnt))
	}
}

func getRandom(seed int64, size int) ([]byte, error) {
	buf := make([]byte, size)

	rnd := rand.New(rand.NewSource(seed))
	if _, err := rnd.Read(buf); err != nil {
		return nil, err
	}

	return buf, nil
}
