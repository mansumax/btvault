package rabin

import (
	"io"

	"github.com/restic/chunker"
)

const (
	kiB = 1024
	miB = 1024 * kiB

	minSize = 512 * kiB // Minimal size of a block.
	maxSize = 8 * miB   // Maximal size of a block.

	poly = chunker.Pol(0x3DA3358B4DC173) // Rabin's polymonial.
)

type Block struct {
	Offset uint
	Length uint
	Digest uint64
	Data   []byte
}

type Slicer struct {
	ch *chunker.Chunker
	rd io.Reader
}

func NewSlicer(rd io.Reader) *Slicer {
	return NewSlicerMinMax(rd, minSize, maxSize)
}

func NewSlicerMinMax(rd io.Reader, min, max uint) *Slicer {
	return &Slicer{
		ch: chunker.NewWithBoundaries(rd, poly, min, max),
		rd: rd,
	}
}

func (s *Slicer) NextBlock() (Block, error) {
	chunk, err := s.ch.Next(nil)
	if err != nil {
		return Block{}, err
	}

	return Block{
		Offset: chunk.Start,
		Length: chunk.Length,
		Digest: chunk.Cut,
		Data:   chunk.Data,
	}, nil
}

func (s *Slicer) Reset(rd io.Reader) {
	s.ch.Reset(rd, poly)
	s.rd = rd
}
