package rscodes

import (
	"bytes"
	"math/rand"
	"testing"
)

func TestSplit(t *testing.T) {
	// Generate ~0.99MiB of deterministic pseudo-random data.
	dataSize := 1023 * 1023
	data, err := getRandom(123, dataSize)
	if err != nil {
		t.Error(err)
	}

	// Create fragmenter.
	fragr, err := NewFragmenter()
	if err != nil {
		t.Error(err)
	}

	// Split the data.
	frags, err := fragr.Split(data)
	if err != nil {
		t.Error(err)
	}

	if len(frags) != 6 {
		t.Error("Number of fragments != 6.")
	}
	if len(frags[0].Data) != 261633 {
		t.Error("Fragment length != 261633.")
	}
	if !bytes.Equal(frags[3].Data[261630:], []byte{0, 0, 0}) {
		t.Error("Last original fragment is not properly padded.")
	}
}

func TestReconstruct(t *testing.T) {
	// Generate ~0.99MiB of deterministic pseudo-random data.
	dataSize := 1023 * 1023
	data, err := getRandom(123, dataSize)
	if err != nil {
		t.Error(err)
	}

	// Create fragmenter.
	fragr, err := NewFragmenter()
	if err != nil {
		t.Error(err)
	}

	// Split the data.
	frags, err := fragr.Split(data)
	if err != nil {
		t.Error(err)
	}

	// Remove data in 2 fragments at random.
	for i := 0; i < 2; i++ {
		frags[rand.Intn(6)].Data = nil
	}

	// Reconstruct data.
	xdata, err := fragr.Reconstruct(frags, dataSize)
	if err != nil {
		t.Error(err)
	}

	if !bytes.Equal(data, xdata) {
		t.Error("Reconstracted data differ from the original data.")
	}
}

func getRandom(seed int64, size int) ([]byte, error) {
	buf := make([]byte, size)

	rnd := rand.New(rand.NewSource(seed))
	if _, err := rnd.Read(buf); err != nil {
		return nil, err
	}

	return buf, nil
}
