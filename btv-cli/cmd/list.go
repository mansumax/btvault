package cmd

import (
	"fmt"
	"log"
	"net/rpc"
	"os"

	"gitlab.fel.cvut.cz/mansumax/btvault/btv-node/node"
	"gitlab.fel.cvut.cz/mansumax/btvault/config"

	"github.com/spf13/cobra"
)

// listCmd represents the list command
var listCmd = &cobra.Command{
	Use:   "list [<id>]",
	Short: "Show the backed up files and their history",
	Long: `The "list" command without any arguments shows a list of the backed up files. If an ID of a specific file is provided, then the history that file will be displayed. This command is designed to be used in conjunction with "restore" command.
Examples:
	Print all available versions of the file with the given ID:
	
	btv-cli list 256BiTsLoNgFiLeHaShInBaSe64FoRmAtBlAbLaBlA
	
	Restore all files from the directory '/home/user/doc':
	
	btv-cli list | awk '/home\/user\/doc/{print $1}' | btv-cli restore --stdin
	
	Write identifiers of all PDF files into a plain text file:
	
	btv-cli list | awk '/\.pdf$/{print $1}' > restore.txt`,
	Run: func(cmd *cobra.Command, args []string) {
		if len(args) > 1 {
			cmd.Usage()
			os.Exit(2)
		}
		if err := runList(&globalOptions, args); err != nil {
			log.Fatalln(err)
		}
	},
}

func init() {
	RootCmd.AddCommand(listCmd)
}

func runList(gopt *GlobalOptions, args []string) error {
	// Load user's config.
	cfg, err := loadConfig(gopt.ConfigFile)
	if err != nil {
		return fmt.Errorf("loading config: %v", err)
	}

	if len(args) > 0 && args[0] != "" {
		res, err := callNodeList(cfg, args[0])
		if err != nil {
			return err
		}
		for _, e := range res.List {
			fmt.Printf("%s    %s\n", e[0], e[1])
		}
	} else {
		res, err := callNodeList(cfg, "")
		if err != nil {
			return err
		}

		for _, e := range res.List {
			fmt.Printf("%s    %s\n", e[0], e[1])
		}
	}

	return nil
}

func callNodeList(cfg *config.Config, id string) (*node.ListResponse, error) {
	client, err := rpc.DialHTTP("tcp", cfg.NodeEndpoint)
	if err != nil {
		return nil, fmt.Errorf("dialing: %v", err)
	}
	req := node.ListRequest{
		ID: id,
	}
	res := node.ListResponse{}
	err = client.Call("NodeRPC.List", req, &res)
	if err != nil {
		return nil, fmt.Errorf("calling node list: %v", err)
	}
	return &res, nil
}
