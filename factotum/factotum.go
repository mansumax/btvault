package factotum

import (
	"bytes"
	"crypto"
	"crypto/aes"
	"crypto/cipher"
	"crypto/ecdsa"
	"crypto/elliptic"
	"crypto/rand"
	"crypto/sha256"
	"fmt"
	"hash"
	"io"
	"io/ioutil"
	"math/big"
	"os"
	"path/filepath"
	"strings"

	"golang.org/x/crypto/hkdf"
)

const (
	aesKeyLen = 32
)

// Factotum is a key manager.
type Factotum struct {
	PublicKey    string
	privateKey   string
	ecdsaKeyPair ecdsa.PrivateKey
}

// Signature is an ECDSA signature.
type Signature struct {
	R, S *big.Int
}

// NewFromDir returns a new Factotum providing all needed private key operations,
// loading keys from a directory containing *.btvkey files.
func NewFromDir(dir string) (*Factotum, error) {
	privBytes, err := readFile(dir, "secret.btvkey")
	if err != nil {
		return nil, err
	}
	privBytes = stripCR(privBytes)
	pubBytes, err := readFile(dir, "public.btvkey")
	if err != nil {
		return nil, err
	}
	pubBytes = stripCR(pubBytes)

	return newFactotum(string(pubBytes), string(privBytes))
}

// newFactotum creates a new Factotum using the given keys.
func newFactotum(public, private string) (*Factotum, error) {
	ecdsaPubKey, err := ParsePublicKey(public)
	if err != nil {
		return nil, err
	}
	ecdsaKeyPair, err := parsePrivateKey(ecdsaPubKey, private)
	if err != nil {
		return nil, err
	}
	return &Factotum{
		PublicKey:    public,
		privateKey:   private,
		ecdsaKeyPair: *ecdsaKeyPair,
	}, nil
}

// PrivateKeyHash runs the specified hash function on the content of the private key
// and returns the resulting hash.Hash instance.
func (f *Factotum) PrivateKeyHash(hf crypto.Hash) hash.Hash {
	h := hf.New()
	h.Write([]byte(f.ecdsaKeyPair.D.Bytes()))
	return h
}

// deriveStrongKey derives a cryptographically strong key from the private key.
func (f *Factotum) deriveStrongKey() ([]byte, error) {
	hash := sha256.New
	master := f.ecdsaKeyPair.D.Bytes()
	h := f.PrivateKeyHash(crypto.SHA256)
	h.Write(elliptic.Marshal(f.ecdsaKeyPair.Curve, f.ecdsaKeyPair.X, f.ecdsaKeyPair.Y))
	info := h.Sum(nil)

	hkdf := hkdf.New(hash, master, nil, info)

	strong := make([]byte, aesKeyLen)
	_, err := io.ReadFull(hkdf, strong)
	if err != nil {
		return nil, err
	}

	return strong, nil
}

// Encrypt encrypts the given plaintext with AES-GCM-256 algorithm using the key
// derived from user's private key. Prepends the output ciphertext with a random
// nonce that was used during encryption.
func (f *Factotum) Encrypt(in []byte) ([]byte, error) {
	// Derive key.
	strong, err := f.deriveStrongKey()
	if err != nil {
		return nil, err
	}

	// Encrypt input.
	bc, err := aes.NewCipher(strong)
	if err != nil {
		return nil, err
	}
	aesgcm, err := cipher.NewGCM(bc)
	if err != nil {
		return nil, err
	}
	// The nonce needs to be unique, but not secure. Therefore it's common to
	// include it at the beginning of the ciphertext.
	out := make([]byte, aesgcm.NonceSize()+len(in))
	// We really should not use more than 2^32 random nonces with a given key
	// because of the risk of a repeat. But since this is only a prototype,
	// we don't really care.
	nonce := out[:aesgcm.NonceSize()]
	if _, err := io.ReadFull(rand.Reader, nonce); err != nil {
		return nil, err
	}
	out = aesgcm.Seal(out[aesgcm.NonceSize():], nonce, in, nil)

	return out, nil
}

// Decrypt decrypts the given ciphertext with AES-GCM-256 algorithm using the key
// derived from user's private key and the provided nonce.
func (f *Factotum) Decrypt(in, nonce []byte) ([]byte, error) {
	// Derive key.
	strong, err := f.deriveStrongKey()
	if err != nil {
		return nil, err
	}

	// Decrypt input.
	bc, err := aes.NewCipher(strong)
	if err != nil {
		return nil, err
	}
	aesgcm, err := cipher.NewGCM(bc)
	if err != nil {
		return nil, err
	}
	out, err := aesgcm.Open(nil, nonce, in, nil)
	if err != nil {
		return nil, err
	}

	return out, nil
}

// Sign signs a slice of bytes with the factotum's private key.
func (f *Factotum) Sign(hash []byte) (*Signature, error) {
	curveLength := (f.ecdsaKeyPair.Curve.Params().N.BitLen() + 7) / 8
	if len(hash) > curveLength {
		return nil, fmt.Errorf("hash is too long to Sign")
	}
	r, s, err := ecdsa.Sign(rand.Reader, &f.ecdsaKeyPair, hash)
	if err != nil {
		return nil, err
	}
	return &Signature{R: r, S: s}, nil
}

// Verify verifies whether the given hash's signature was signed by the private
// key corresponding to the given public key.
func Verify(hash []byte, sig Signature, public string) error {
	ecdsaPubKey, err := ParsePublicKey(public)
	if err != nil {
		return err
	}
	if !ecdsa.Verify(ecdsaPubKey, hash, sig.R, sig.S) {
		return fmt.Errorf("signature does not match")
	}
	return nil
}

// ParsePublicKey takes an BTVault's representation of a public key and converts it
// into an ECDSA public key.
func ParsePublicKey(public string) (*ecdsa.PublicKey, error) {
	fields := strings.Split(public, "\n")
	if len(fields) != 4 { // 4 is because string should be terminated by \n, hence fields[3]==""
		return nil, fmt.Errorf("expected keytype, two big ints and a newline; got %d %v", len(fields), fields)
	}
	keyType := fields[0]
	var x, y big.Int
	_, ok := x.SetString(fields[1], 10)
	if !ok {
		return nil, fmt.Errorf("%s is not a big int", fields[1])
	}
	_, ok = y.SetString(fields[2], 10)
	if !ok {
		return nil, fmt.Errorf("%s is not a big int", fields[2])
	}

	var curve elliptic.Curve
	switch keyType {
	case "p256":
		curve = elliptic.P256()
	case "p521":
		curve = elliptic.P521()
	case "p384":
		curve = elliptic.P384()
	default:
		return nil, fmt.Errorf("unknown key type: %q", keyType)
	}
	return &ecdsa.PublicKey{Curve: curve, X: &x, Y: &y}, nil
}

// parsePrivateKey returns an ECDSA private key given a user's ECDSA public key and a
// string representation of the private key.
func parsePrivateKey(publicKey *ecdsa.PublicKey, private string) (*ecdsa.PrivateKey, error) {
	var d big.Int
	err := d.UnmarshalText([]byte(clean(private)))
	if err != nil {
		return nil, err
	}
	x, y := publicKey.Curve.ScalarBaseMult(d.Bytes())
	if x.Cmp(publicKey.X) != 0 || y.Cmp(publicKey.Y) != 0 {
		return nil, fmt.Errorf("public and private keys do not correspond")
	}
	return &ecdsa.PrivateKey{PublicKey: *publicKey, D: &d}, nil
}

// readFile reads the given file in the chosen directory.
func readFile(dir, name string) ([]byte, error) {
	b, err := ioutil.ReadFile(filepath.Join(dir, name))
	if os.IsNotExist(err) {
		return nil, err
	}
	if err != nil {
		return nil, err
	}
	return b, nil
}

// stripCR removes \r.
func stripCR(b []byte) []byte {
	return bytes.Replace(b, []byte("\r"), []byte(""), -1)
}

// clean removes comments and starting and leading space.
func clean(s string) string {
	if i := strings.IndexByte(s, '#'); i >= 0 {
		s = s[:i]
	}
	return strings.TrimSpace(s)
}
