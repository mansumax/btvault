#!/bin/bash
# This script will prepare the environment for demonstration/testing.
# It will start the server and populate the network with nodes.

# Terminate on failure.
set -e

# Install programs.
echo -n "installing necessary programs ... "
go install gitlab.fel.cvut.cz/mansumax/btvault/...
echo "done"

# Add executables to PATH.
PATH=${PATH}:${GOPATH}/bin

# Set variables.
MIN_USERS=0
MAX_USERS=${1:-10}
PIDS=()

# Create temporary directory.
echo -n "creating temporary directory ... "
TMP_DIR=/tmp/btvault-$(date '+%Y%m%d%H%M%S')
mkdir -p ${TMP_DIR}
echo "done [${TMP_DIR}]"

# Create directory for logs.
echo -n "creating log directory ..."
LOG_DIR=${TMP_DIR}/logs
mkdir -p ${LOG_DIR}
echo "done [${LOG_DIR}]"

# Start server.
echo -n "starting server ... "
SRV_DIR=${TMP_DIR}/srv
mkdir -p ${SRV_DIR}
btv-cli keygen \
	--keydir=${SRV_DIR}/keys \
	--secretseed='zimut-kunid-lirus-mugah-hitot-jibil-mojup-judan' \
	>/dev/null
btv-srv \
	--keydir=${SRV_DIR}/keys \
	--srvdir=${SRV_DIR} \
	--verbose \
	&>${LOG_DIR}/btv-srv.log &
PIDS+=("$!")
echo "done [${PIDS[-1]}]"

# Wait for server to setup.
sleep 3

# Populate the network.
for ((i=MIN_USERS; i<MAX_USERS; i++))
do
	# Create node.
	USER_NAME="user$(printf '%03d' ${i})"
	USER_DIR=${TMP_DIR}/${USER_NAME}
	mkdir -p ${USER_DIR}
	echo | btv-cli signup \
		--config=${USER_DIR}/config.yaml \
		--keydir=${USER_DIR}/keys \
		--repo=${USER_DIR}/repo \
		${USER_NAME}@example.com \
		>/dev/null
	echo "added user: ${USER_NAME}"

	# Start node.
	echo -n "starting node ... "
	btv-node \
		--config=${USER_DIR}/config.yaml \
		--no-rpc \
		--verbose \
		&>${LOG_DIR}/${USER_NAME}.log &
	PIDS+=("$!")
	echo "done [${PIDS[-1]}]"
done

# Wait for user's input.
echo
echo -n "press ENTER to terminate the session"
read

# Clean up.
echo -n "cleanup up ... "
kill -INT ${PIDS[*]}
rm -rf ${TMP_DIR}
echo "done"
