# BTVault
BTVault  is a peer-to-peer backup system based on BitTorrent protocol.

## Installation
```
$ go get -u gitlab.fel.cvut.cz/mansumax/btvault/...
```

## Author
Maxat Mansurov (mansumax@fel.cvut.cz)
