package main

import (
	"crypto/sha1"
	"crypto/sha256"
	"encoding/base64"
	"fmt"
	"io"
	"net/http"
	"net/http/httputil"
	"strings"

	"gitlab.fel.cvut.cz/mansumax/btvault/btv-srv/db"
	"gitlab.fel.cvut.cz/mansumax/btvault/factotum"

	"github.com/asaskevich/govalidator"
)

// confirmHandler implements an http.Handler that handles user creation requests.
type confirmHandler struct {
	fact *factotum.Factotum
	db   *db.SQLiteDB
}

// newSignupHandler creates a new handler that serves /signup.
func newConfirmHandler(fact *factotum.Factotum, db *db.SQLiteDB) *confirmHandler {
	return &confirmHandler{
		fact: fact,
		db:   db,
	}
}

func (h *confirmHandler) ServeHTTP(w http.ResponseWriter, r *http.Request) {
	errorf := func(code int, format string, args ...interface{}) {
		http.Error(w, fmt.Sprintf(format, args...), code)
	}

	// Save a copy of this request for debugging.
	dump, err := httputil.DumpRequest(r, true)
	if err != nil {
		errorf(http.StatusInternalServerError, "dumping request: %v", err)
		return
	}
	Verbosef(string(dump))

	// Parse and validate request.
	if r.Method != "POST" {
		errorf(http.StatusMethodNotAllowed, "method not allowed")
		return
	}
	userName := r.FormValue("name")
	publicKey := r.FormValue("key")
	sigR := r.FormValue("sigR")
	sigS := r.FormValue("sigS")
	confCode := r.FormValue("code")
	if !govalidator.IsEmail(userName) {
		errorf(http.StatusBadRequest, "invalid user name: %s", userName)
		return
	}
	if err := verifySignupSignature(userName, publicKey, sigR, sigS); err != nil {
		errorf(http.StatusBadRequest, "invalid request: %s", err)
		return
	}

	//TODO Lookup confCode in DB. It should exist.

	// Generate user id.
	hash := sha1.New()
	io.WriteString(hash, userName)
	io.WriteString(hash, confCode)
	sum1 := hash.Sum(nil)
	id := base64.RawURLEncoding.EncodeToString(sum1[:])

	// Generate user certificate.
	hash = sha256.New()
	io.WriteString(hash, userName)
	io.WriteString(hash, publicKey)
	sum2 := hash.Sum(nil)
	sig, err := h.fact.Sign(sum2[:])
	if err != nil {
		errorf(http.StatusInternalServerError, "failed to create certificate")
		return
	}
	cert := sig.R.String() + "," + sig.S.String()

	// Insert new user into DB.
	user := db.User{
		UserID:      id,
		UserName:    userName,
		PublicKey:   publicKey,
		Certificate: cert,
	}
	if err = h.db.InsertUser(user); err != nil {
		errorf(http.StatusInternalServerError, "db error: %v", err)
		return
	}

	// Send response.
	res := strings.Join([]string{id, cert, h.fact.PublicKey}, "&")
	io.WriteString(w, res)
}
