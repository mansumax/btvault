package main

import (
	"crypto/sha256"
	"encoding/binary"
	"fmt"
	"math/big"
	"net/http"
	"net/http/httputil"
	"net/url"

	"gitlab.fel.cvut.cz/mansumax/btvault/btv-srv/db"
	"gitlab.fel.cvut.cz/mansumax/btvault/factotum"
)

// broadcastHandler implements an http.Handler that handles distribute requests.
type broadcastHandler struct {
	fact *factotum.Factotum
	db   *db.SQLiteDB
}

// newBroadcastHandler creates a new handler that serves /broadcast.
func newBroadcastHandler(fact *factotum.Factotum, db *db.SQLiteDB) *broadcastHandler {
	return &broadcastHandler{
		fact: fact,
		db:   db,
	}
}

func (h *broadcastHandler) ServeHTTP(w http.ResponseWriter, r *http.Request) {
	errorf := func(code int, format string, args ...interface{}) {
		http.Error(w, fmt.Sprintf(format, args...), code)
	}

	// Save a copy of this request for debugging.
	dump, err := httputil.DumpRequest(r, true)
	if err != nil {
		errorf(http.StatusInternalServerError, "dumping request: %v", err)
		return
	}
	Verbosef(string(dump))

	// Parse and validate request.
	if r.Method != "POST" {
		errorf(http.StatusMethodNotAllowed, "method not allowed")
		return
	}
	senderID := r.FormValue("sender")
	publicKey := r.FormValue("key")
	msg := r.FormValue("msg")
	sigR := r.FormValue("sigR")
	sigS := r.FormValue("sigS")
	if err := verifyBroadcastSignature(senderID, publicKey, msg, sigR, sigS); err != nil {
		errorf(http.StatusBadRequest, "invalid request: %s", err)
		return
	}

	// Select random users from DB.
	recips, err := h.db.SelectUsersByRandom(1)
	if err != nil {
		errorf(http.StatusInternalServerError, "db error: %v", err)
		return
	}

	// Broadcast the message.
	for _, recip := range recips {
		if recip.UserID == senderID {
			continue
		}
		if err = sendMessage(recip, msg); err != nil {
			errorf(http.StatusInternalServerError, "failed to send the message: %v", err)
			return
		}
	}

	fmt.Fprintln(w, "OK")
}

// verifyBroadcastSignature verifies that the broadcast request was signed by
// the user using the private key that corresponds to the public key provided.
func verifyBroadcastSignature(senderID, publicKey, msg, sigR, sigS string) error {
	var rs, ss big.Int
	if _, ok := rs.SetString(sigR, 10); !ok {
		return fmt.Errorf("invalid signature R value")
	}
	if _, ok := ss.SetString(sigS, 10); !ok {
		return fmt.Errorf("invalid signature S value")
	}
	sig := factotum.Signature{R: &rs, S: &ss}
	hash, _ := broadcastRequestHash(senderID, publicKey, msg)

	return factotum.Verify(hash, sig, publicKey)
}

// broadcastRequestHash generates a hash of the supplied arguments
// that, when signed, is used to prove that a broadcast request originated
// from the user that owns the supplied private key.
func broadcastRequestHash(senderID, publicKey, msg string) ([]byte, url.Values) {
	const magic = "broadcast-request"
	u := url.Values{}
	h := sha256.New()
	h.Write([]byte(magic))
	w := func(key, val string) {
		var l [4]byte
		binary.BigEndian.PutUint32(l[:], uint32(len(val)))
		h.Write(l[:])
		h.Write([]byte(val))
		u.Add(key, val)
	}
	w("sender", senderID)
	w("key", publicKey)
	w("msg", msg)

	return h.Sum(nil), u
}
