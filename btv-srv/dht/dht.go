package dht

import (
	"bufio"
	"log"
	"net"
	"os"
	"path/filepath"
	"strings"
)

const (
	dataDir = "dht"
)

var Commands = map[string]func(*DHT, net.Conn, []string) error{
	"PUT": (*DHT).put,
	"GET": (*DHT).get,
}

type DHT struct {
	dataDir string
	port    string
}

func InitDHT(dir, port string) (*DHT, error) {
	path := filepath.Join(dir, dataDir)
	_, err := os.Stat(path)
	if err != nil && !os.IsNotExist(err) {
		return nil, err
	}
	if err := os.MkdirAll(path, 0774); err != nil {
		return nil, err
	}
	return &DHT{
		dataDir: path,
		port:    port,
	}, nil
}

func (dht *DHT) Run() {
	l, err := net.Listen("tcp", ":"+dht.port)
	if err != nil {
		log.Fatalln("listen error:", err)
	}
	defer l.Close()

	for {
		conn, err := l.Accept()
		if err != nil {
			log.Fatalln("accept error:", err)
		}
		go dht.handleConnection(conn)
	}
}

func (dht *DHT) handleConnection(conn net.Conn) {
	defer conn.Close()

	msg, err := bufio.NewReader(conn).ReadString('\n')
	if err != nil {
		log.Printf("reading from connection: %v", err)
		return
	}

	fields := strings.Split(strings.TrimSpace(msg), " ")
	if len(fields) == 0 {
		log.Printf("ivalid message: %q", msg)
		return
	}
	cmd := strings.ToUpper(fields[0])
	if f, ok := Commands[cmd]; ok {
		if err := f(dht, conn, fields[1:]); err != nil {
			log.Printf("%s: %v", cmd, err)
		}
		return
	}
	log.Printf("unknown command: %q", cmd)
}
