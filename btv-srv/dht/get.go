package dht

import (
	"errors"
	"fmt"
	"io"
	"net"
	"os"
	"path/filepath"
)

func (dht *DHT) get(conn net.Conn, args []string) error {
	if len(args) != 1 {
		return errors.New("wrong number of arguments")
	}
	id := args[0]

	// Open the requested file.
	path := filepath.Join(dht.dataDir, id)
	f, err := os.Open(path)
	if err != nil {
		fmt.Fprintf(conn, "KO %q\n", err)
		if os.IsNotExist(err) {
			return nil
		}
		return err
	}
	defer f.Close()

	// Get the file size.
	fi, err := f.Stat()
	if err != nil {
		fmt.Fprintf(conn, "KO %q\n", err)
		return err
	}

	// Send the file size.
	fmt.Fprintf(conn, "OK %d\n", fi.Size())

	// Send the file.
	_, err = io.Copy(conn, f)

	return err
}
