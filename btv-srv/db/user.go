package db

import (
	"database/sql"
	"time"
)

type User struct {
	UserID           string
	UserName         string
	PublicKey        string
	Certificate      string
	IP               string
	Port             string
	Reputation       float64
	registrationDate time.Time
	lastUpdateDate   time.Time
}

func createUsersTable(db *sql.DB) error {
	stmt := `
	CREATE TABLE IF NOT EXISTS Users(
		UserID           TEXT NOT NULL PRIMARY KEY,
		UserName         TEXT NOT NULL UNIQUE,
		PublicKey        TEXT NOT NULL,
		Certificate      TEXT NOT NULL,
		IP               TEXT DEFAULT "127.0.0.1",
		Port             TEXT DEFAULT "1234",
		Reputation       FLOAT DEFAULT 0.5,
		RegistrationDate TIMESTAMP NOT NULL,
		LastUpdateDate   TIMESTAMP NOT NULL
	);
	`
	_, err := db.Exec(stmt)
	return err
}

func (s *SQLiteDB) SelectUserByUserID(userID string) (User, error) {
	stmt := `
	SELECT * FROM Users
	WHERE UserID=?;
	`
	row := s.db.QueryRow(stmt, userID)
	return processUsersRow(row)
}

func (s *SQLiteDB) SelectUserByUserName(userName string) (User, error) {
	stmt := `
	SELECT * FROM Users
	WHERE UserName=?;
	`
	row := s.db.QueryRow(stmt, userName)
	return processUsersRow(row)
}

func (s *SQLiteDB) SelectUsersByRandom(limit int) ([]User, error) {
	stmt := `
	SELECT * FROM Users
	ORDER BY RANDOM()
	LIMIT ?;
	`
	rows, err := s.db.Query(stmt, limit)
	if err != nil {
		return nil, err
	}
	return processUsersRows(rows)
}

func (s *SQLiteDB) UpdateUser(user User) error {
	stmt := `
	UPDATE Users
	SET PublicKey=?, Certificate=?, IP=?, Port=?, Reputation=?, LastUpdateDate=?
	WHERE UserID=?;
	`
	_, err := s.db.Exec(
		stmt,
		user.PublicKey,
		user.Certificate,
		user.IP,
		user.Port,
		user.Reputation,
		time.Now(),
		user.UserID,
	)
	return err
}

func (s *SQLiteDB) UpdateUsers(users []User) error {
	for _, user := range users {
		if err := s.UpdateUser(user); err != nil {
			return err
		}
	}
	return nil
}

func (s *SQLiteDB) InsertUser(user User) error {
	stmt := `
	INSERT INTO Users (
		UserID,
		UserName,
		PublicKey,
		Certificate,
		RegistrationDate,
		LastUpdateDate
	)
	VALUES (?, ?, ?, ?, ?, ?);
	`
	now := time.Now()
	_, err := s.db.Exec(
		stmt,
		user.UserID,
		user.UserName,
		user.PublicKey,
		user.Certificate,
		now,
		now,
	)
	return err
}

func (s *SQLiteDB) InsertUsers(users []User) error {
	for _, user := range users {
		if err := s.InsertUser(user); err != nil {
			return err
		}
	}
	return nil
}

func processUsersRow(row *sql.Row) (User, error) {
	us := User{}
	err := row.Scan(
		&us.UserID,
		&us.UserName,
		&us.PublicKey,
		&us.Certificate,
		&us.IP,
		&us.Port,
		&us.Reputation,
		&us.registrationDate,
		&us.lastUpdateDate,
	)
	if err != nil {
		return User{}, err
	}
	return us, nil
}

func processUsersRows(rows *sql.Rows) (res []User, err error) {
	for rows.Next() {
		us := User{}
		err = rows.Scan(
			&us.UserID,
			&us.UserName,
			&us.PublicKey,
			&us.Certificate,
			&us.IP,
			&us.Port,
			&us.Reputation,
			&us.registrationDate,
			&us.lastUpdateDate,
		)
		if err != nil {
			return nil, err
		}
		res = append(res, us)
	}
	if err = rows.Err(); err != nil {
		return nil, err
	}
	return
}
