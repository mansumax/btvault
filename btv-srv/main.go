package main

import (
	"fmt"
	"log"
	"net"
	"net/http"
	"os"
	"os/signal"
	"path/filepath"

	"gitlab.fel.cvut.cz/mansumax/btvault/btv-srv/db"
	"gitlab.fel.cvut.cz/mansumax/btvault/btv-srv/dht"
	"gitlab.fel.cvut.cz/mansumax/btvault/factotum"
	"gitlab.fel.cvut.cz/mansumax/btvault/utils"

	homedir "github.com/mitchellh/go-homedir"

	"github.com/asaskevich/govalidator"
	"github.com/spf13/cobra"
)

const (
	defPort    = "7771"
	defDHTPort = "7773"
	defKeyDir  = ".ssh"
	defSrvDir  = ".btvault/srv"
)

// GlobalOptions holds values for global flags.
type GlobalOptions struct {
	Verbose bool
	Port    string
	DHTPort string
	KeyDir  string
	SrvDir  string
}

var globalOptions GlobalOptions

// MainCmd represents the base command when called without any subcommands.
var mainCmd = &cobra.Command{
	Use:   "btv-srv",
	Short: "A central server of the BTVault system.",
	Long:  `This application represents a central server of the BTVault system.`,
	Run: func(cmd *cobra.Command, args []string) {
		if err := runMain(&globalOptions, args); err != nil {
			log.Fatalln(err)
		}
	},
}

func main() {
	if err := mainCmd.Execute(); err != nil {
		fmt.Println(err)
		os.Exit(-1)
	}
}

func init() {
	cobra.OnInitialize(initLog)

	pfs := mainCmd.PersistentFlags()

	pfs.StringVar(&globalOptions.Port, "port", defPort, "port `number` for server to listen for incoming requests")
	pfs.StringVar(&globalOptions.DHTPort, "dhtport", defDHTPort, "port `number` for DHT to listen for incoming requests")
	pfs.StringVar(&globalOptions.KeyDir, "keydir", "", "`directory` where the keys are stored (default is $HOME/.ssh)")
	pfs.StringVar(&globalOptions.SrvDir, "srvdir", "", "main server `directory` (default is $HOME/.btvault/srv)")
	pfs.BoolVarP(&globalOptions.Verbose, "verbose", "v", false, "verbose mode")
}

func runMain(opt *GlobalOptions, args []string) error {
	// Check port numbers.
	if !govalidator.IsPort(opt.Port) {
		return fmt.Errorf("invalid port number %s", opt.Port)
	}
	if !govalidator.IsPort(opt.DHTPort) {
		return fmt.Errorf("invalid port number %s", opt.DHTPort)
	}

	// Verify keys directory.
	if opt.KeyDir == "" {
		home, err := homedir.Dir()
		if err != nil {
			return err
		}
		opt.KeyDir = filepath.Join(home, defKeyDir)
	}
	absKeyDir, err := utils.AbsPathify(opt.KeyDir)
	if err != nil {
		return fmt.Errorf("%s: %v", opt.KeyDir, err)
	}
	opt.KeyDir = absKeyDir

	// Verify server directory.
	if opt.SrvDir == "" {
		home, err := homedir.Dir()
		if err != nil {
			return err
		}
		opt.SrvDir = filepath.Join(home, defSrvDir)
	}
	absSrvDir, err := utils.AbsPathify(opt.SrvDir)
	if err != nil {
		return fmt.Errorf("%s: %v", opt.SrvDir, err)
	}
	opt.SrvDir = absSrvDir

	// Create a factotum.
	fact, err := factotum.NewFromDir(opt.KeyDir)
	if err != nil {
		return fmt.Errorf("creating factotum: %v", err)
	}

	// Init database.
	db, err := db.InitDB(opt.SrvDir)
	if err != nil {
		return fmt.Errorf("initializing database: %v", err)
	}

	// Init DHT.
	dht, err := dht.InitDHT(opt.SrvDir, opt.DHTPort)
	if err != nil {
		return fmt.Errorf("initializing DHT: %v", err)
	}

	// Start DHT.
	go dht.Run()

	// Setup handlers.
	http.Handle("/signup", newSignupHandler(fact, db))
	http.Handle("/confirm", newConfirmHandler(fact, db))
	http.Handle("/heartbeat", newHeartbeatHandler(fact, db))
	http.Handle("/send", newSendHandler(fact, db))
	http.Handle("/broadcast", newBroadcastHandler(fact, db))
	http.Handle("/reputation", newReputationHandler(fact, db))

	// Start server.
	addr := net.JoinHostPort("0.0.0.0", opt.Port)
	srv := http.Server{Addr: addr}
	go srv.ListenAndServe()
	defer srv.Shutdown(nil)

	// Wait for SIGINT to shutdown.
	c := make(chan os.Signal, 1)
	signal.Notify(c, os.Interrupt)
	<-c
	log.Printf("caught SIGINT.")
	log.Printf("shutting down.")

	return nil
}

// initLog initializes the standard logger.
func initLog() {
	log.SetFlags(0)
	log.SetPrefix("bvt-server: ")
}

// Verbosef printfs the given message when the verbose flag is set.
func Verbosef(format string, args ...interface{}) {
	if !globalOptions.Verbose {
		return
	}

	log.Printf(format, args...)
}
