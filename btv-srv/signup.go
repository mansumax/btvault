package main

import (
	"bytes"
	"crypto/rand"
	"crypto/sha256"
	"database/sql"
	"encoding/binary"
	"fmt"
	"io"
	"log"
	"math/big"
	"net/http"
	"net/http/httputil"
	"net/url"

	"gitlab.fel.cvut.cz/mansumax/btvault/btv-srv/db"
	"gitlab.fel.cvut.cz/mansumax/btvault/factotum"

	"github.com/asaskevich/govalidator"
)

// signupHandler implements an http.Handler that handles user creation requests.
type signupHandler struct {
	fact *factotum.Factotum
	db   *db.SQLiteDB
}

// newSignupHandler creates a new handler that serves /signup.
func newSignupHandler(fact *factotum.Factotum, db *db.SQLiteDB) *signupHandler {
	return &signupHandler{
		fact: fact,
		db:   db,
	}
}

func (h *signupHandler) ServeHTTP(w http.ResponseWriter, r *http.Request) {
	errorf := func(code int, format string, args ...interface{}) {
		http.Error(w, fmt.Sprintf(format, args...), code)
	}

	// Save a copy of this request for debugging.
	dump, err := httputil.DumpRequest(r, true)
	if err != nil {
		errorf(http.StatusInternalServerError, "dumping request: %v", err)
		return
	}
	Verbosef(string(dump))

	// Parse and validate request.
	if r.Method != "POST" {
		errorf(http.StatusMethodNotAllowed, "method not allowed")
		return
	}
	userName := r.FormValue("name")
	publicKey := r.FormValue("key")
	sigR := r.FormValue("sigR")
	sigS := r.FormValue("sigS")
	if !govalidator.IsEmail(userName) {
		errorf(http.StatusBadRequest, "invalid user name: %s", userName)
		return
	}
	if err := verifySignupSignature(userName, publicKey, sigR, sigS); err != nil {
		errorf(http.StatusBadRequest, "invalid request: %s", err)
		return
	}

	// Check if userName is available.
	_, err = h.db.SelectUserByUserName(userName)
	if err != nil && err != sql.ErrNoRows {
		errorf(http.StatusInternalServerError, "db error: %v", err)
		return
	}
	if err != sql.ErrNoRows {
		errorf(http.StatusBadRequest, "user name is already taken: %s", userName)
		return
	}

	// Send signup confirmation mail to user.
	if err := sendConfirmationEmail(userName); err != nil {
		log.Printf("error sending mail to %q: %v", userName, err)
		errorf(http.StatusInternalServerError, "could not send signup email")
		return
	}
	fmt.Fprintln(w, "OK")
}

// verifySignupSignature verifies that the new user record comprised were
// properly signed by the new user using the private key that corresponds
// to the public key provided.
func verifySignupSignature(userName, publicKey, sigR, sigS string) error {
	var rs, ss big.Int
	if _, ok := rs.SetString(sigR, 10); !ok {
		return fmt.Errorf("invalid signature R value")
	}
	if _, ok := ss.SetString(sigS, 10); !ok {
		return fmt.Errorf("invalid signature S value")
	}
	sig := factotum.Signature{R: &rs, S: &ss}
	hash, _ := signupRequestHash(userName, publicKey)

	return factotum.Verify(hash, sig, publicKey)
}

// signupRequestHash generates a hash of the supplied arguments
// that, when signed, is used to prove that a signup request originated
// from the user that owns the supplied private key.
func signupRequestHash(userName, publicKey string) ([]byte, url.Values) {
	const magic = "signup-request"
	u := url.Values{}
	h := sha256.New()
	h.Write([]byte(magic))
	w := func(key, val string) {
		var l [4]byte
		binary.BigEndian.PutUint32(l[:], uint32(len(val)))
		h.Write(l[:])
		h.Write([]byte(val))
		u.Add(key, val)
	}
	w("name", userName)
	w("key", publicKey)

	return h.Sum(nil), u
}

// sendConfirmationEmail send confirmation email to the user.
func sendConfirmationEmail(dst string) error {
	confCode, err := genConfirmationCode()
	if err != nil {
		return err
	}

	var body bytes.Buffer
	fmt.Fprintln(&body, "Enter this confirmation code to complete BTVault signup process:")
	fmt.Fprintln(&body, string(confCode))
	fmt.Fprintln(&body, "\nIf you were not expecting this message, please ignore it.")

	log.Printf("sending email to %s: %s\n", dst, body.String())

	return nil
}

var table = [...]byte{'1', '2', '3', '4', '5', '6', '7', '8', '9', '0'}

func genConfirmationCode() (string, error) {
	b := make([]byte, 6)
	n, err := io.ReadAtLeast(rand.Reader, b, 6)
	if n != 6 {
		return "", err
	}
	for i := 0; i < len(b); i++ {
		b[i] = table[int(b[i])%len(table)]
	}
	return string(b), nil
}
