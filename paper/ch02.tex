\chapter{Návrh systému BTVault}
Tato kapitola popisuje návrh zálohovacího systému BTVault.
Napříč celou kapitolou budeme dodržovat následující terminologii:
\begin{description}[align=left]
	\item [Uživatel] člověk používající systém.
	\item [Klient] front-end systému používaný uživatelem.
	\item [Uzel] počítač, na kterém běží instance BTVaultu.
	\item [Smlouva] dvoustranná dohoda mezi uzly sítě.
	\item [Partner] jeden z účastníků smlouvy.
\end{description}

\section{Základní rysy aplikace}

\subsection{Zálohování}
Uživatel může pomocí klienta vybrat data, které je potřeba zazálohovat. Vybraná data jsou zašifrována a rozdělena do fragmentů. Pro úspěšné zálohování vybraných dat musí uzel najít dostatečné množství \uv{úložného prostoru} v Peer-to-Peer (P2P) síti. Uzel získává \uv{úložný prostor} obchodováním volným prostorem na svém pevném disku s ostatními uzly P2P sítě. Uživatel určuje horní limit pro množství diskového prostoru dostupného k obchodování. Jakmile se uzlu podaří získat potřebné množství \uv{úložného prostoru}, dojde k distribuci vytvořených fragmentů. Celý proces zálohování je mnohem detailněji popsán v Sekci \ref{sec:backup}.

\subsection{Obnovení zálohy}
Po úspěšném vytvoření zálohy si uživatel může pomocí klienta nechat zobrazit veškeré uložené soubory spolu s jim přiřazenými unikátními identifikačními čísly. Uživatel si muže také prohlédnout všechny dostupné verze jednotlivých souborů. Tímto způsobem je uživatel schopen specifikovat přesně která data je potřeba obnovit. Proces obnovení každého vybraného souboru probíhá následovně: klient nejdříve určí umístění jednotlivých fragmentů patřících danému souboru, pak tyto fragmenty stáhne, dešifruje a složí je dohromady. Celý proces znovuobnovení dat je mnohem detailněji popsán v Sekci \ref{sec:restoration}.

\subsection{Optimální obchodování}
Pro zaručení férovosti vztahů mezi uzly sítě se v BTVaultu používá jednoduchý reputační systém, který by měl chránit systém proti zneužití černými pasažéry a zlomyslnými uzly. Uzly sítě během obchodování vždy berou v úvahu reputaci svých potenciálních partnerů, což se pak odráží v kapacitě nabízeného diskového prostoru. Při sjednávaní partnerství mezi dvěma uzly se případný reputační rozdíl kompenzuje poskytnutím dodatečného diskového prostoru uzlem s nižší reputaci. Tímto způsobem máme garantováno, že kvalita služeb poskytovaných uzlům sítě je přímo úměrná celkové přínosnosti těchto uzlů v rámci systému. Detailnější popis reputačního systému a celého procesu obchodování lze najít v Sekci \ref{sec:trading}.

\subsection{Bezpečnost}
Jelikož se uživatelská data uchovávají na pevných discích vzdálených počítačů, je důležité zaručit jejich zabezpečení a omezit k nim přístup nežádoucím osobám. Z tohoto důvodu se veškerá uživatelská data před odesláním šifrují, a to takovým způsobem, že dešifrovat je schopen pouze jejich majitel. Více informací o bezpečnostním modelu najdete v Sekci \ref{sec:security}.

\subsection{Dostupnost dat}
Aby byli uživatelé systému schopni vytvářet pravidelné zálohy a poskytovat zálohovací služby ostatním členům sítě, musí být přítomni v systému poměrně často. Pokud uživatel není dostupen již delší dobu, jeho partneři se mohou rozhodnout jej potrestat smazáním jeho záloh. Dále BTVault používá mechanismus pro ověření toho, že všechny uzly opravdu splňuji své povinnosti a uchovávají svěřená jím data. Za nesplnění nebo porušení povinností uzlům hrozí trest ze strany jejich partnerů. Více informací o bezpečnostním modelu najdete v Sekci \ref{sec:availability}.

\section{Optimální obchodování} \label{sec:trading}
\subsection{Reputační systém}
Reputace odráží kvalitu daného uzlu z hlediska ostatních uzlů sítě. Kvalitou uzlu v daném případě myslíme míru toho, jak často je daný uzel \textit{on-line} tj. je připojen k Internetu a poskytuje zálohovací služby. Informace o reputacích všech uzlů P2P sítě se uchovává na centrálním serveru a je dostupná k nahlédnutí všem uživatelům systému.
\par
Uzel může odvodit reputaci jiných uzlů dvěma způsoby: buď přímým pozorováním, nebo tázáním centrálního serveru. Začneme prvním případem, kdy uzel $i$ odhaduje reputaci uzlu $j$ na základě vlastních zkušeností. Dejme tomu, že uzel $j$ uchovává u sebe na disku zálohu uzlu $i$. Pak může uzel $i$ periodicky posílat uzlu $j$ různé zkoušky (\textit{challenges}), čímž ověří, že uzel $j$ je \textit{on-line} a že opravdu uchovává u sebe potřebnou zálohu. Záznamy o úspěšnosti těchto zkoušek budou zapsány do lokální databáze uzlu $i$. Pokud celkový počet interakci mezi uzly $i$ a $j$ označíme jako $t_{ij}$ a počet úspěšných interakci označíme jako $s_{ij}$, můžeme spočítat reputaci uzlu $j$ z hlediska uzlu $i$ podle vzorce:
\begin{equation}
d_{ij} = \frac{s_{ij}}{t_{ij}}
\end{equation}
\par
Uzel $i$ může také získat reputaci uzlu $j$ od serveru. Tento způsob se používá v případech, kdy se uzly $i$ a $j$ setkávají poprvé. Všechny uzly periodicky posílají na server informaci o svých zkušenostech s ostatními uzly sítě. Například, uzel $a$ může poslat informaci o svém partnerovi $b$ ve formátu $\langle d_{ab}$, $t_{ab} \rangle$. Pak je server schopen dopočítat reputaci uzlu $j$ z informací získaných od jeho partnerů $P$ podle následujícího vzorce:
\begin{equation}
e_{j} = 
\begin{cases}
\frac{\sum\limits_{k \in P} e_{k} d_{kj} t_{kj}}{\sum\limits_{k \in P} t_{kj}}, &  \text{pokud } \sum\limits_{k \in P} t_{kj} \ge 10\\
0.5, &  \text{jinak (t.j. uzel } j \text{ je nový)}\\
\end{cases}
\end{equation}
\par
Uzel $i$ pak může z $d_{ij}$ a $e_{j}$ spočítat celkovou reputaci uzlu $j$ podle vzorce:
\begin{equation}
r_{ij} = \alpha d_{ij} + (1 - \alpha) e_{j}, \text{ kde } \alpha = \min\{\frac{t_{ij}}{n}, 1\},
\end{equation}
kde $n$ je minimální počet interakcí potřebných k tomu, aby uzel $i$ byl schopen samostatně soudit o chování uzlu $j$. Číslo $n$ musí být dostatečně velké, aby uzel $i$ měl možnost získat dostatečné zkušenosti s uzlem $j$ (momentálně $n=100$).

\subsection{Optimální obchodní politika}
Předpokládejme, že se uzel $A$ s reputací $r_A$ chce stát partnerem uzlu $B$ s reputací $r_B$. Potřebujeme zjistit, jakou velikost prostoru $s_B$ musí získat uzel $A$ od uzlu $B$ v případě, že uzel $A$ nabízí prostor velikostí $s_A$. Pokud mají uzly $A$ a $B$ stejnou reputaci (tj. $r_A = r_B$), oba uzly obdrží prostory stejné velikosti $s_A$. Pravděpodobnost toho, že některý z uzlu není \textit{on-line} je $1 - r_A$. Pravděpodobnost toho, že žádný z uzlů není \textit{on-line}, je rovna $(1 - r_A)^2$. Odtud můžeme odvodit, že dostupnost zálohy je v tomto případě rovna
\begin{equation}
R_{(r_A = r_B)} = 1 - (1 - r_A)^2.
\end{equation}
Dostupností zálohy zde myslíme pravděpodobnost toho, že v daný okamžik je požadovaná záloha dostupná buď z uzlu $A$, nebo z uzlu $B$.
\par
Pokud má uzel $B$ nižší reputaci než uzel $A$ (tj. $r_A > r_B$), uzel $A$ musí jako kompenzaci dostat vetší prostor pro uložení zálohy. V takovém případě $s_B = k s_A$, kde $k \ge 1$. Dostupnost zálohy se bude rovnat
\begin{equation}
R_{(r_A > r_B)} = 1 - (1 - r_A)(1 - r_B)^k.
\end{equation}
\par
Jelikož potřebujeme zaručit aby $R_{(r_A = r_B)} = R_{(r_A > r_B)}$, dostáváme následující rovnici:
\begin{equation}
1 - (1 - r_A)^2 = 1 - (1 - r_A)(1 - r_B)^k.
\end{equation}
Odkud po vyřešení pro neznámé $k$ dostáváme
\begin{equation}
k = \log_{(1-r_B)}(1-r_A).
\end{equation}
Optimální obchodní politika pro uzel A vypadá tudíž následovně:
\begin{equation} \label{eq:optimal-trading}
s_B = s_A  \log_{(1-r_B)}(1-r_A),
\end{equation}
Vzorec \ref{eq:optimal-trading} lze také zapsat jako:
\begin{equation}
\frac{s_B}{s_A} = \frac{\log(1-r_A)}{\log(1-r_B)}.
\end{equation}

\subsection{Obchodní schéma}
Pro uložení svých záloh každý uzel potřebuje získat určité množství \uv{úložného prostoru} v síti. Uživatele mohou přispět svým lokálním volným místem na disku do systému výměnou za spolehlivé úložiště jejich dat u jiných uživatelů. Přičemž poměr mezi množstvím poskytnutého prostoru a získaného prostoru závisí na reputaci daných uživatelů a určuje se podle vzorce \ref{eq:optimal-trading}.
\par
Uživatelé specifikují, jakou část volného prostoru na pevných discích jsou ochotni poskytnout ostatním členům sítě a uzly si pak mezi sebou obchodují diskovým prostorem v souladu s obchodním protokolem. Podle tohoto protokolu se každé obchodní jednání uskutečňuje mezi dvěma uzly P2P sítě. Výsledkem úspěšně ukončeného jednání je \textit{smlouva}, která specifikuje kapacitu diskového prostoru alokovaného každou stranou pro potřeby jiné strany. Mezi dvěma uzly může být uzavřeno nekonečné množství podobných smluv.
\par
Například, pokud má uzel $A$ sjednanou smlouvu $c$ s jiným uzlem $B$, uzel $A$ má právo použit diskový prostor poskytnutý uzlem $B$ v rámci smlouvy $c$. Potom může uzel $A$ může poslat uzlu $B$ svoje data $d$ ve tvaru $\langle d,c \rangle$, čímž naznačí použití smlouvy $c$. V tomto případě budou data $d$ propojena se smlouvou $c$.
\par
Smlouvy se vždycky uzavírají na dobu určitou s možností prodloužení. Ukončením platnosti smlouvy se ukončuje i povinnost stran uchovávat data spojená s touto smlouvou.

\section{Organizace dat v síti} \label{sec:data-organization}

\subsection{Správa záloh a metadat}
V systému BTVault spravujeme data dvou typu: samotná uživatelská data a speciální meta-soubory sloužící k nalezení zazálohovaných dat v síti. Jak samotná data, tak i metadata se ukládají do sítě. Podstatným rozdílem je ovšem to, že data se uchovávají přímo v souborových systémech partnerských uzlů, kdežto metadata se vkládají do distribuované hašovací tabulky (DHT) tvořené všemi uzly sítě.
\par
Hlavní výhodou takového přístupu je minimalizace síťové komunikace mezi uzly a vyhnutí se režijním nákladům spojeným s častou migraci dat. Pokud bychom totiž do DHT ukládali kromě metadat ještě i data samotná, jak to dělá například pStore, museli bychom pak přeposílat stovky megabajtů pokaždé když dojde ke vzniku/zániku nějakého uzlu \cite{Blake:2003:HAS:1251054.1251055}. Na druhou stranu je velikost metadat většinou velice malá (v poměru se samotnými daty skoro zanedbatelná), takže si můžeme dovolit použití DHT pro jejich ukládání. Kromě toho uživatelé systému potřebují mít neustálý přístup ke svým metadatům, protože metadata obsahují informaci o všech uživatelských zálohách a vztazích s jinými členy sítě. Uložením metadat do DHT jí zároveň přenecháváme veškeré starosti spojené se zajištěním non-stop dostupností těchto dat (jejich replikaci a případnou migraci).

\subsection{Organizace metadat}
BTVault udržuje několik typu metadat. Uživatelé systému potřebují mít informaci o zazálohovaných souborech a umístění fragmentů patřících těmto souborům. Uzly také potřebují mít neustálý přístup k informaci o uzavřených smlouvách s jinými uzly sítě. Jak již bylo vysvětleno výše, metadata obsahující tyto informace se ukládají do DHT. Tato subsekce popisuje jak jsou tyto metadata strukturovaná a v jaké podobě se uchovávají uvnitř DHT.

\subsubsection{Struktura metadat}
Meta-data jsou strukturována do několika vrstev. Zavedení více vrstev umožňuje uživatelům vytahovat potřebnou informaci z DHT postupně, po malých částech. Kdybychom totiž veškerou uživatelskou meta-informaci uchovávali v jednom velkém meta-souboru, museli bychom pak vytahovat/vkládat z/do DHT celý meta-soubor, bez ohledu na to, jakou velkou část z té meta-informace uživatel vlastně potřebuje. Mimo jiné, vícevrstvá struktura metadat umožňuje jejich sdílení mezi uzly.
\par
Je třeba ovšem poznamenat, že podobné strukturování má i určité nevýhody. Hlavní nevýhodou je zvýšení počtu vyhledávání v DHT potřebných pro přístup k informacím obsazených v nižších vrstvách. Tento problém se částečně řeší lokálním cachováním metadat přímo v souborových systémech uzlů. Jiným nedostatkem vícevrstvé struktury je potřeba zajištění konzistence metadat napříč všemi vrstvami. Jinými slovy, vnesení jakýchkoliv úprav v jedné vrstvě často vynucuje aktualizaci metadat i na vyšších vrstvách. Je patrné, že závažnost těchto problémů roste přímo úměrně s počtem zavedených vrstev.
\par
Ve snaze minimalizovat výše zmíněné nevýhody jsme v BTVaultu zavedli čtyřvrstvé strukturování metadat. První vrstva uchovává dva druhy uživatelské informace: seznam zazálohovaných souborů (\textit{file list}) a seznam smluv (\textit{contract list}). Ve druhé vrstvě udržujeme seznamy bloků souborů (\textit{file block lists}) a uživatelské smlouvy (\textit{contracts}). Do třetí vrstvy vkládáme seznamy fragmentů bloků (\textit{block fragment lists}). Čtvrtá vrstva obsahuje seznamy držitelů fragmentů (\textit{holder lists}). Obecný přehled takového strukturování metadat je zobrazen na Obrázku \ref{fig:data-organization}. Veškerá metadata se ukládají do DHT v podobě dvojic klíč-hodnota $\langle k, v \rangle$. Dále následuje detailnější popis jednotlivých druhů metadat:
\begin{description}
	\item[File list (FL)] \hfill \\
		obsahuje výčet všech uživatelem zazálohovaných souborů v podobě jejich absolutních cest.
		Do DHT se vkládá v následující podobě \footnote{\usedNotation}:
		\begin{equation*}
			\langle k,v \rangle = \langle \hash(\mathit{K'} \concat U \concat \text{sůl}), \ \mathit{IV} \concat \aesgcm(\mathit{FL}, \derive(\mathit{K'}), \mathit{IV}) \rangle.
		\end{equation*}
	\item[Contract list (CL)] \hfill \\
		obsahuje seznam identifikačních čísel všech uživatelských smluv.
		Do DHT se vkládá v následující podobě:
		\begin{equation*}
		\langle k,v \rangle = \langle \hash(\mathit{K'} \concat U \concat \text{sůl}), \ \mathit{IV} \concat \aesgcm(\mathit{CL}, \derive(\mathit{K'}), \mathit{IV}) \rangle.
		\end{equation*}
	\item[File block list (FBL)] \hfill \\
		obsahuje následující informace o souboru: název, velikost, přístupová práva a seznam dostupných verzí. Každá verze se skládá z data vytvoření zálohy a seznamu s informací (hash, umístění uvnitř souboru a velikost) o všech blocích patřících dané verzi souboru.
		Do DHT se vkládá v následující podobě:
		\begin{equation*}
		\langle k,v \rangle = \langle \hash(\mathit{K'} \concat \text{soubor}), \ \mathit{IV} \concat \aesgcm(\mathit{FBL}, \derive(\mathit{K'}), \mathit{IV}) \rangle.
		\end{equation*}
	\item[Contract (C)] \hfill \\
		obsahuje následující informace o smlouvě: identifikační čísla obou stan, sjednané kapacity vyměněných diskových prostorů, seznamy fragmentů spojených s konkretní smlouvou.
		Do DHT se vkládá v následující podobě:
		\begin{equation*}
		\langle k,v \rangle = \langle \hash(\mathit{K'} \concat \text{smlouva}), \ \mathit{IV} \concat \aesgcm(\mathit{C}, \derive(\mathit{K'}), \mathit{IV}) \rangle.
		\end{equation*}
	\item[Block fragment list (BFL)] \hfill \\
		obsahuje seznam s informací (hash, umístění uvnitř bloku) o všech fragmentech patřících danému bloku.
		Do DHT se vkládá v následující podobě:
		\begin{equation*}
		\langle k,v \rangle = \langle \hash(\hash(\text{blok})), \ \aesctr(\mathit{BFL}, \hash(\text{blok}), \zerovec) \rangle.
		\end{equation*}
	\item[Holder list (HL)] \hfill \\
		obsahuje seznam s identifikačními čísly všech držitelů daného fragmentů.
		Do DHT se vkládá v následující podobě:
		\begin{equation*}
		\langle k,v \rangle = \langle \hash(\hash(\text{fragment})), \ \aesctr(\mathit{HL}, \hash(\text{fragment}), \zerovec) \rangle.
		\end{equation*}
\end{description}
\begin{figure}[t]
	\centering
	\includegraphics[width=\textwidth]{img/data-organization.png}
	\caption{Organizace dat v systému BTVault.}
	\label{fig:data-organization}
\end{figure}

\subsection{Organizace záloh}
V BTVaultu se uživatelský soubor typicky neuchovává v síti jako celek, místo toho se rozdistribuovává po celé síti v podobě malých ($< 1$MB) fragmentů. Hlavní přínos tohoto přístupu je v tom, že umožňuje použití efektivních technik pro řešení problému s dostupnosti dat. Kupříkladu je zřejmé, že je mnohem efektivnější replikovat malé fragmenty  do několika uzlů v síti, než je tomu v případě celých souborů, jejichž velikost není nijak omezená.
\par
Další výhodou rozdělení souborů do fragmentů je to, že nám dovoluje zrychlit proces obnovení záloh, a to díky malé velikosti samotných fragmentů. Umožňuje nám totiž paralelizovat celý proces stahování dat ze sítě, tedy jsme schopni současně stahovat několik fragmentů od několika uzlů.
\par
Dále, při nasekání dat do menších fragmentů existuje určitá možnost, že někdy dojde k vytvoření identických fragmentů. Její pravděpodobnost je tím větší, čím menší je velikost vytvářených fragmentů. Identické fragmenty pak mohou být uložené v síti pouze jednou, a jejich vlastníci je tak mohou efektivně sdílet mezi sebou.
\par
V BTVaultu se snažíme maximálně využít všech výše uvedených výhod, proto se celý proces fragmentace uživatelských dat provádí ve dvou fázích:
\begin{enumerate}
	\item Rozdělíme každý soubor do několika bloků různé velikosti.
	\item Rozdělíme každý z bloků vytvořených v 1. fázi do čtyř fragmentů fixní velikosti.
\end{enumerate}
Dále následuje detailnější rozbor každé fáze.

\subsubsection{Rozdělení souborů do bloků}
V první fázi se nasekání souborů provádí na základě jejich obsahu. Používáme k tomu tzv. metodu posuvného okna \cite{Muthitacharoen:2001:LNF:502059.502052} (\textit{sliding window algorithm}). Tato metoda je užitečná tím, že vytváří bloky, které jsou odolné vůči posunům (\textit{shift-resistant}) uvnitř souborů, což výrazně usnadňuje identifikaci duplicitních bloků během inkrementálního zálohování. Příklad takového rozdělení je názorně ilustrován na obrázku \ref{fig:content-based-slicing}.
\begin{figure}[t]
	\centering
	\includegraphics[width=0.65\textwidth]{img/content-based-slicing.png}
	\caption{Vývoj bloků souboru \texttt{Soubor.txt} mezi jeho různými verzemi. Horizontální proužky demonstrují koncové posloupnosti 48 bajtů. Šedou barvou jsou označeny změněné části souboru.}
	\label{fig:content-based-slicing}
\end{figure}
Samotný proces rozdělení do bloků pak probíhá podle následujícího algoritmu: 
\begin{enumerate}
	\item Definujeme kladné celé číslo $d$, které bude reprezentovat střední velikost bloků (např. pro bloky velikosti $\approx$ 2MB použijeme $d = 2^{11}$).
	\item Definujeme pohyblivé okno fixní délky $m$ bajtů (typicky $m = 48$) a nastavíme jej na začátek souboru.
	\item Pro danou posloupnost bajtů uvnitř pohyblivého okna $P = [\, p_0, p_1, \ldots, p_{m-1} \,]$:
	\begin{enumerate}
		\item Vypočteme hodnotu Rabinova otisku \cite{rabin1981fingerprinting} (\textit{Rabin Fingerprint}) posloupnosti $P$ podle vzorce:
		\[
		\rabin(P) = \rabin([\, p_0, p_1, \ldots, p_{m-1} \,]) = (\sum_{i = 0}^{m-1} q^{m-(i+1)} p_i) \bmod d,
		\]
		kde $q$ je náhodně vybrané prvočíslo.
		\item Pokud se hodnota $\rabin(P)$ rovná $d - 1$, označíme posloupnost $P$ jako hranici bloku a ořízneme soubor na pozici $p_{m-1}$.
	\end{enumerate}
	\item Pokud jsme na konci souboru, ukončíme běh algoritmu.
	\item Jinak posuneme okno o jeden bajt dále a skočíme na krok č. 3.
\end{enumerate}
Je zřejmé, že výše uvedený algoritmus má časovou složitost $O(mn)$, kde $n$ je velikost zpracovávaného souboru v bajtech. Jelikož ale používáme Rabinovy otisky, v praxi lze tento algoritmus implementovat tak, aby ve většině případů běžel mnohem rychleji. Například pokud si na začátku algoritmu předpočteme hodnoty $q^{k}$ pro všechny možné $k \in \{0, \dots, 255\}$, zpracovávaní souboru velikosti 1GB na moderních procesorech zabere jen pár desítek milisekund.

\subsubsection{Rozdělení bloků do fragmentů}
Ve druhé fázi se každý blok souboru velikosti $s$ bajtů rozdělí do čtyř fragmentů fixní velikosti $\lceil \frac{s}{4} \rceil$ bajtů, přičemž případné zbylé místo v posledním fragmentu vyplníme nulovými bajty. Fixní velikost fragmentů nám pak umožní použití sofistikovanějších algoritmů pro zvýšení spolehlivosti systému a zajištění dostupnosti záloh. Podrobný popis těchto technik najdete v Sekci \ref{sec:availability}.

\section{Bezpečnost} \label{sec:security}
Jelikož se veškerá data v P2P sítích uchovávají na pevných discích nedůvěryhodných uzlů, potřebujeme tyto data nějakým způsobem ochránit. V BTVaultu se všechna uživatelská data před odesláním šifrují, a to takovým způsobem, že dešifrování těchto dat je schopen provést jedině jejich majitel.
\par
Každý uživatel systému BTVault vlastní jeden šifrovací klíč, který se skládá ze dvou části: veřejné a soukromé. Veřejná část klíče se odesílá do centrálního serveru při registraci a je veřejně přístupná. Veřejné části se především používají k ověření digitálních podpisů vygenerovaných pomocí odpovídajících soukromých částí.
\par
Jak již bylo naznačeno v Sekci \ref{sec:data-organization}, privátní uživatelská metadata se šifrují algoritmem AES-GCM \cite{Dworkin:2007:SRB:2206251}, který zajišťuje jejich důvěrnost, integritu a autentizaci. Šifrovací klíč se v tomto případě odvozuje z privátní části uživatelského klíče pomocí funkce HDKF \cite{Krawczyk:2010:CEK:1881412.1881456}. Inicializační vektor se generuje náhodně a na konci se připojuje k zašifrovaným datům.
\par
Samotná uživatelská data a sdílená metadata se šifrují algoritmem AES-CTR \cite{Dworkin:2001:SER:2206247}. Šifrovací klíč se odvozuje přímo ze šifrovaných dat pomocí kryptografické hašovací funkce (např. SHA-2). Takovému přístupu se v odborné literatuře ještě někdy říká konvergentní šifrování \cite{convergent-encryption1022312} (\textit{convergent encryption}). Fakt, že se šifrovací klíč generuje přímo z obsahu dat, přináší ihned několik výhod:
\begin{itemize}
	\item Schopnost správně dešifrovat data implikuje jejich integritu.
	\item Umožňuje sdílení stejných dat mezi uzly (resp. deduplikace dat), jelikož identické soubory jsou zašifrovány stejně.
\end{itemize}

\section{Dostupnost dat} \label{sec:availability}
Jak již bylo zmíněno v Sekci \ref{sec:data-organization}, veškerá metadata se vkládají do DHT. Tím pádem všechny záležitosti spojené s udržováním metadat a zaručením jejich nepřetržité dostupnosti přenecháváme DHT. DHT zajišťuje dostupnost metadat udržením ihned několika replik těchto metadat a jejich případným přemístěním z méně spolehlivých uzlů do více spolehlivých.
\par
Jelikož se ovšem samotná uživatelská data neukládají do DHT, o jejich dostupnost se má postarat systém BTVault. Obecně v P2P sítích není možné nikdy 100\% garantovat neustálou dostupnost všech uchovávaných dat, a to kvůli dynamické podstatě podobných sítí. Je totiž poměrně těžké předpovědět chování a stabilitu jednotlivých uzlů, obzvlášť pokud se jedná o relativně \uv{nové} členy sítě. Proto v BTVaultu pro zvýšení spolehlivosti systému a snížení pravděpodobnosti ztráty dat používáme kombinaci dvou nejpopulárnějších technik zabezpečení dat: samoopravných kódů (\textit{erasure codes}) a klasické replikace.
\par
Ze Sekce \ref{sec:data-organization} víme, že uživatelská data se nejdříve dělí do bloků, a každý blok se následně rozděluje do čtyř fragmentů fixní velikosti. Z těchto čtyř fragmentů se pomocí Reed-Solomonových \cite{reed60polynomial} (RS) kódů generují další dva paritní fragmenty, jak je ukázáno na Obrázku \ref{fig:blocks-to-fragments}. Velká výhoda RS kódů spočívá v tom, že umožňují obnovení původního obsahu každého bloku z libovolné 4-členné kombinace jeho fragmentů. Jinými slovy pro každý konkrétní blok jsme schopni tolerovat ztrátu libovolných dvou fragmentů patřících danému bloku.
\begin{figure}[t]
	\centering
	\includegraphics[width=0.8\textwidth]{img/blocks-to-fragments.png}
	\caption{Příklad rozdělení bloku $b_1$ do fragmentů.}
	\label{fig:blocks-to-fragments}
\end{figure}
\par
Tento přístup ovšem má i jednu očividnou nevýhodu: pokud dojde ke ztrátě nějakého fragmentů v síti, jeho obnovení bude vyžadovat přístup ke všem zbylým fragmentům bloku. A jelikož v P2P sítích ztráta dat je nevyhnutelná a docela běžná situace, potřebujeme mnohem efektivnější způsob, jak se vypořádat se ztrácenými fragmenty. Jedním z možných řešení tohoto problému je replikace fragmentů do několika uzlů sítě. V systému BTVault udržujeme $k$ (momentálně $k=5$) replik všech fragmentů, přičemž tolerujeme možnou ztrátu $i < k$ (momentálně $i=2$) replik jednotlivých fragmentu. V drtivé většině případu se ztráta fragmentů bude řešit jednoduchým přeposláním kopií již existujících replik. Jen ve velice řídkých případech, kdy dojde k současné ztrátě všech $k$ replik, se budeme muset uchýlit k \uv{dražšímu} způsobu obnovení obsahu fragmentu vyžadujícího stáhnutí všech ostatních fragmentů bloku.

\subsection{Kontrolování partnerů}
Abychom mohli zaručit dostupnost uživatelských dat v síti, potřebujeme neustále kontrolovat jejich existenci a dosažitelnost. V BTVaultu pro tyto účely používáme zkouškový mechanismus (\textit{challenge mechanism}), pomocí kterému všechny uzly jsou schopni kontrolovat své partnery. Tento mechanismus slouží především k ověřování toho, zda všichni partneři opravdu splňují svoje smluvní povinnosti a uchovávají jím svěřená data.
\par
Zkoušky se vytvářejí během procesu zálohování dat, před samotnou distribuci dat do partnerských uzlů. Nechť uzel $A$ chce uložit množinu fragmentů $F$ na disku uzlu $B$ na základe smlouvy $c$. Pak je seznam zkoušek $Z$ vygenerován podle následujícího algoritmu:
\begin{enumerate}
	\item Vybereme náhodnou neprázdnou variaci fragmentů $V$ z množiny $F$.
	\item Vygenerujeme náhodné číslo $r$.
	\item Spočteme hash $H(V, r)$ podle následujícího rekurentního vzorce \footnote{\usedNotationOne}:
	\begin{equation}
		H(V, r) = 
		\begin{cases}
			r, \text{ pokud } \len(V) = 0, \\
			\hash(\head(V) \concat H(\tail(V), r)), \text{ jinak}. \\
		\end{cases}
	\end{equation}
	\item Přidáme trojici $\langle V, r, H(V, r)) \rangle$ do seznamu $Z$.
	\item Pokud je počet elementů v $Z$ roven $n$, ukončíme běh algoritmu.
	\item Jinak skočíme na krok č. 1.
\end{enumerate}
Ve výše uvedeném algoritmu číslo $n$ musí být dostatečně velké, aby nedocházelo k opakovanému použití zkoušek (momentálně $n=100$).
\par
Samotné kontrolování partneru pak probíhá následujícím způsobem: uzel $A$ periodický vybírá nějakou dvojici $\langle V, r \rangle$ ze seznamu $Z$ a žádá svého partnera $B$ o výpočet $H(V, r)$. Žádost se odesílá v podobě trojice $\langle V, r, c \rangle$. Tímto způsobem bude uzel $B$ schopen ověřit pravost požadavku. Uzel $A$ pak porovnává odpověď svého partnera s hodnotou $H(V, r)$ uloženou v $Z$. Jak již bylo zmíněno v Sekci \ref{sec:trading}, záznamy o úspěšnosti těchto pravidelných kontrol se zapisují do lokální databáze uzlu. Pokud není uzel $B$ delší dobu schopen prokázat plnění svých smluvních povinnosti (ať již z důvodu nedostupnosti nebo nezvadnutím zkoušek), tak mu hrozí trest ze strany uzlu $A$. Uzel $A$ se totiž může rozhodnout vypovědět uzavřenou mezi uzly smlouvu $c$ a smazat případnou zálohu uzlu $B$ z disku. Neslušné chování uzlu $B$ se také odrazí na jeho celkové reputaci v systému. Nízká reputace nedovolí uzlu $B$ efektivně obchodovat s jinými uzly sítě, což mu zabrání v navázání dalších partnerství.

\section{Proces zálohování} \label{sec:backup}
Uživatel zahájí proces zálohovaní spuštěním klientské aplikace. Zálohovací proces se skládá z několika etap:
\begin{enumerate}
	\item Výběr souborů k zálohování.
	\item Rozdělení souborů do bloků.
	\item Deduplikace bloků.
	\item Rozdělení bloků do fragmentů.
	\item Připravení zálohovacích sad.
	\item Distribuce zálohovacích sad do partnerských uzlů.
\end{enumerate}
Tyto etapy se provádějí sekvenčně. Dále následuje podrobný popis každé etapy.

\subsubsection{Výběr souborů k zálohování}
Než začneme samotný proces zálohování, potřebujeme nejdříve identifikovat soubory ve sledovaném adresáři, které se změnily od předešlé inkrementální zálohy. To provedeme porovnáním času poslední změny souborů s časem vytvoření jejich minulých záloh. Informaci o všech dostupných verzích pro konkretní soubor lze dohledat v příslušných metadatech. Pokud je zjištěno, že poslední verze byla vytvořená ještě před tím, než došlo k úpravě souboru, označíme soubor jako změněný a obnovíme jeho zálohu. V opačném případě budeme považovat soubor za nezměněný a vyřadíme jej z dalšího zpracovávaní.

\subsubsection{Rozdělení souborů do bloků}
Nejdříve potřebujeme nasekat velké ($>$ 1 MB) soubory na několik bloků menší velikosti. Rozdělení do bloků provedeme pro každý soubor zvlášť. Každý soubor se rozdělí do bloků na základě jeho obsahu, k čemuž použijeme Rabinovy otisky. Informaci o všech blocích souboru (hash, velikost, umístění uvnitř souboru) zapíšeme do speciálního meta-souboru \textit{File Block List} (FBL).

\subsubsection{Deduplikace bloků}
Dále následuje proces deduplikace bloků. Nejprve se pro každý blok $b$ zkontroluje existence jemu odpovídajícího meta-souboru \textit{block fragment list} (BFL) v DHT. Ze Sekce \ref{sec:data-organization} víme, že se BFL vkládají do DHT s klíčem $k = \hash(\hash(b))$. Bloky s již existujícími BFL vyřadíme z dalšího zpracovávání.

\subsubsection{Rozdělení bloků do fragmentů}
Každý ze zbylých bloků pak rozdělíme do čtyř fragmentů fixní velikosti. K těmto čtyřem fragmentům pak pomocí samoopravných Reed-Solomonových kódů vygenerujeme dva paritní fragmenty. Informaci o původních čtyřech fragmentech bloku (hash, velikost, umístění uvnitř bloku) a o dvou paritních fragmentech (hash, velikost) zapíšeme do příslušného BFL. Všechny fragmenty pak zašifrujeme pomocí konvergentního šifrování. 

\subsubsection{Připravení zálohovacích sad}
\begin{figure}[t]
	\centering
	\includegraphics[width=0.7\textwidth]{img/fragments-to-sets.png}
	\caption{Příklad vytvoření zálohovacích sad}
	\label{fig:fragments-to-sets}
\end{figure}
Vytvořené fragmenty jsou pak rozděleny do šesti zálohovacích sad, jak je ukázáno na Obrázku \ref{fig:fragments-to-sets}. Tyto zálohovací sady jsou následně rozdistribuovány do partnerských uzlů.

\subsubsection{Distribuce zálohovacích sad do partnerských uzlů}
Jak bylo zmíněno v Sekci \ref{sec:trading}, uzly v systému BTVault používají optimální politiku obchodování. Celý průběh procesu obchodování je zobrazen na Obrázku \ref{fig:trading}. Uzel, který chce zazálohovat svá data, nejdříve rozešle po celé síti žádost o obchodování (\textit{trading request}). Tato žádost bude obsahovat následující informaci: identifikační číslo žádajícího uzlu a velikost potřebného diskového prostoru. Pokud podobnou žádost obdrží uzel, který má prostředky a chuť obchodovat, odešle žádajícímu uzlu svůj návrh partnerské smlouvy. Tento návrh bude obsahovat následující informaci: identifikační číslo navrhujícího uzlu, identifikační číslo smlouvy, velikost diskového prostoru požadovanou navrhujícím uzlem (určuje se podle vzorce \ref{eq:optimal-trading}), dobu platnosti smlouvy a digitální podpis navrhovatele. Jakmile tento návrh obdrží původní žadatel, může rozhodnout, zda tuto nabídku přijme nebo zamítne. Pokud žádající uzel návrh schválí, na základě tohoto návrhu vytvoří novou smlouvu $c$. Již podepsaná kopie $c$ se odešle zpět navrhovateli. Tento proces se bude opakovat do té doby, dokud zálohující uzel nesežene dostatečné množství partnerů pro uchování všech připravených zálohovacích sad.
\par
Ihned po skončení procesu obchodování následuje proces přenosu uživatelských dat do partnerských uzlu. Pro každou zálohovací sadu $S = [s_1, \dots, s_6]$ najdeme $k$ (momentálně $k=5$) smluv $C = [c_1, \dots, c_k]$ odpovídající velikosti. Samotný přenos sad se uskuteční pomocí BitTorrent protokolu, takže se pro každou sadu vytvoří jeden torrent soubor. Tím pádem je $i$-tá zálohovací sada odeslána $j$-tému partnerovi v podobě $\langle \torrent(s_i), c_j \rangle$, čímž jsou propojeny všechny fragmenty sady $s_i$ se smlouvou $c_j$. Každý partner bude zapsán do \textit{holder list} (HL) jako držitel všech jemu svěřených fragmentů.
\begin{figure}[t]
	\centering
	\includegraphics[width=\textwidth]{img/trading.png}
	\caption{Příklad komunikace mezi uzly během procesu distribuce dat. Přerušovanými šipkami je vyznačena komunikace prostřednictvím DHT. Obyčejné šipky označují přímé soketové spojení mezi uzly.}
	\label{fig:trading}
\end{figure}

\section{Proces obnovení dat} \label{sec:restoration}
Stejně jako u zálohování se proces obnovení dat spouští pomocí klientské aplikace. K obnovení obsahu konkretního souboru nejdříve potřebujeme dohledat v DHT meta-soubor příslušný danému souboru. FBL tohoto souboru obsahuje informaci o všech blocích patřících danému souboru. Pomocí FBL jsme schopni zrekonstruovat původní obsah souboru z obsahu jeho bloků. Informace obsažená v FBL nám mimo jiné dovoluje najít BFL potřebných bloků. Z BFL zjistíme veškerou informací o fragmentech bloku. Tato informace nám umožní dešifrovat obsah fragmentů a najít jejich HL, které nám poskytnou informaci o umístění fragmentů v síti.
\par
Pro stažení fragmentu $f$ odešleme uzlům za něj zodpovědným žádost o jeho stažení. Tato žádost bude obsahovat pouze identifikační číslo požadovaného fragmentu. Po obdržení této žádosti uzel vyhledá ve svém souborovém systému požadovaný fragment a pošle jej žádajícímu uzlu. Tento proces je názorně ilustrován na Obrázku \ref{fig:restoration}.
\begin{figure}[t]
	\centering
	\includegraphics[width=0.45\textwidth]{img/restore.png}
	\caption{Příklad komunikace mezi uzly během procesu obnovení dat. Přerušovanými šipkami je vyznačena komunikace prostřednictvím DHT. Obyčejné šipky označují přímé soketové spojení mezi uzly.}
	\label{fig:restoration}
\end{figure}
