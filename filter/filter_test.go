package filter

import (
	"testing"
)

var matchTests = []struct {
	pattern string
	path    string
	match   bool
}{
	{"", "", true},
	{"", "foo", true},
	{"", "/x/y/z/foo", true},
	{"*.go", "/foo/bar/test.go", true},
	{"*.c", "/foo/bar/test.go", false},
	{"*", "/foo/bar/test.go", true},
	{"foo*", "/foo/bar/test.go", true},
	{"bar*", "/foo/bar/test.go", true},
	{"/bar*", "/foo/bar/test.go", false},
	{"bar/*", "/foo/bar/test.go", true},
	{"baz/*", "/foo/bar/test.go", false},
	{"bar/test.go", "/foo/bar/test.go", true},
	{"bar/*.go", "/foo/bar/test.go", true},
	{"ba*/*.go", "/foo/bar/test.go", true},
	{"bb*/*.go", "/foo/bar/test.go", false},
	{"test.*", "/foo/bar/test.go", true},
	{"tesT.*", "/foo/bar/test.go", false},
	{"bar/*", "/foo/bar/baz", true},
	{"bar", "/foo/bar", true},
	{"/foo/bar", "/foo/bar", true},
	{"/foo/bar/", "/foo/bar", true},
	{"/foo/bar", "/foo/baz", false},
	{"/foo/bar", "/foo/baz/", false},
	{"/foo///bar", "/foo/bar", true},
	{"/foo/../bar", "/foo/bar", false},
	{"/foo/../bar", "/bar", true},
	{"/foo", "/foo/baz", true},
	{"/foo/", "/foo/baz", true},
	{"/foo/*", "/foo", false},
	{"/foo/*", "/foo/baz", true},
	{"bar", "/foo/bar/baz", true},
	{"bar", "/foo/bar/test.go", true},
	{"/foo/*test.*", "/foo/bar/test.go", false},
	{"/foo/*/test.*", "/foo/bar/test.go", true},
	{"/foo/*/bar/test.*", "/foo/bar/test.go", false},
	{"/*/*/bar/test.*", "/foo/bar/test.go", false},
	{"/*/*/bar/test.*", "/foo/bar/baz/test.go", false},
	{"/*/*/baz/test.*", "/foo/bar/baz/test.go", true},
	{"/*/foo/bar/test.*", "/foo/bar/baz/test.go", false},
	{"/*/foo/bar/test.*", "/foo/bar/baz/test.go", false},
	{"/foo/bar/test.*", "bar/baz/test.go", false},
	{"/x/y/bar/baz/test.*", "bar/baz/test.go", false},
	{"/x/y/bar/baz/test.c", "bar/baz/test.go", false},
	{"baz/test.*", "bar/baz/test.go", true},
	{"baz/tesT.*", "bar/baz/test.go", false},
	{"test.go", "bar/baz/test.go", true},
	{"*.go", "bar/baz/test.go", true},
	{"*.c", "bar/baz/test.go", false},
	{"sdk", "/foo/bar/sdk", true},
	{"sdk", "/foo/bar/sdk/test/sdk_foo.go", true},
}

func testpattern(t *testing.T, pattern, path string, shouldMatch bool) {
	match, err := Match(pattern, path)
	if err != nil {
		t.Errorf("test pattern %q failed: expected no error for path %q, but error returned: %v", pattern, path, err)
	}

	if match != shouldMatch {
		t.Errorf("test: Match(%q, %q): expected %v, got %v", pattern, path, shouldMatch, match)
	}
}

func TestMatch(t *testing.T) {
	for _, test := range matchTests {
		testpattern(t, test.pattern, test.path, test.match)
	}
}

var filterListTests = []struct {
	patterns []string
	path     string
	match    bool
}{
	{[]string{"*.go"}, "/foo/bar/test.go", true},
	{[]string{"*.c"}, "/foo/bar/test.go", false},
	{[]string{"*.go", "*.c"}, "/foo/bar/test.go", true},
	{[]string{"*"}, "/foo/bar/test.go", true},
	{[]string{"x"}, "/foo/bar/test.go", false},
	{[]string{"?"}, "/foo/bar/test.go", false},
	{[]string{"?", "x"}, "/foo/bar/x", true},
	{[]string{"/*/*/bar/test.*"}, "/foo/bar/test.go", false},
	{[]string{"/*/*/bar/test.*", "*.go"}, "/foo/bar/test.go", true},
}

func TestMatchList(t *testing.T) {
	for i, test := range filterListTests {
		match, err := MatchList(test.patterns, test.path)
		if err != nil {
			t.Errorf("test %d failed: expected no error for patterns %q, but error returned: %v", i, test.patterns, err)
			continue
		}

		if match != test.match {
			t.Errorf("test %d: MatchList(%q, %q): expected %v, got %v", i, test.patterns, test.path, test.match, match)
		}
	}
}
