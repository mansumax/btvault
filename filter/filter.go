package filter

import (
	"fmt"
	"path/filepath"
	"strings"
)

// Match returns true if string matches the pattern.
func Match(pat, str string) (bool, error) {
	if pat == "" {
		return true, nil
	}

	pat = filepath.Clean(pat)

	if str == "" {
		return false, fmt.Errorf("filter.Match: string is empty")
	}

	pats := strings.Split(pat, "/")
	strs := strings.Split(str, "/")

	return match(pats, strs)
}

// MatchList returns true if string matches one of the patterns.
func MatchList(pats []string, str string) (bool, error) {
	for _, pat := range pats {
		matched, err := Match(pat, str)
		if err != nil {
			return false, err
		}

		if matched {
			return true, nil
		}
	}

	return false, nil
}

func match(pats, strs []string) (matched bool, err error) {
	if len(pats) == 0 && len(strs) == 0 {
		return true, nil
	}

	if len(pats) <= len(strs) {
	outer:
		for offset := len(strs) - len(pats); offset >= 0; offset-- {
			for i := len(pats) - 1; i >= 0; i-- {
				ok, err := filepath.Match(pats[i], strs[offset+i])
				if err != nil {
					return false, fmt.Errorf("filter: %v", err)
				}
				if !ok {
					continue outer
				}
			}

			return true, nil
		}
	}

	return false, nil
}
